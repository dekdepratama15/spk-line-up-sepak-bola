-- -------------------------------------------------------------
-- TablePlus 5.3.0(486)
--
-- https://tableplus.com/
--
-- Database: spk_sepak_bola
-- Generation Time: 2024-03-30 19:16:06.6480
-- -------------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


DROP TABLE IF EXISTS `kriteria`;
CREATE TABLE `kriteria` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama_kriteria` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `keep` decimal(15,2) DEFAULT '0.00',
  `defend` decimal(15,2) DEFAULT '0.00',
  `mid` decimal(15,2) DEFAULT '0.00',
  `attack` decimal(15,2) DEFAULT '0.00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `latihan`;
CREATE TABLE `latihan` (
  `id` int NOT NULL AUTO_INCREMENT,
  `kriteria_id` int DEFAULT NULL,
  `pelatih_id` int DEFAULT NULL,
  `nama_latihan` varchar(255) DEFAULT NULL,
  `jam_selesai` datetime DEFAULT NULL,
  `jam_mulai` datetime DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `latihan_pemain`;
CREATE TABLE `latihan_pemain` (
  `id` int NOT NULL AUTO_INCREMENT,
  `latihan_id` int DEFAULT NULL,
  `pemain_id` int DEFAULT NULL,
  `nilai` decimal(15,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=82 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `lineup`;
CREATE TABLE `lineup` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama_lineup` varchar(255) DEFAULT NULL,
  `periode_id` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `lineup_pemain`;
CREATE TABLE `lineup_pemain` (
  `id` int NOT NULL AUTO_INCREMENT,
  `lineup_id` int DEFAULT NULL,
  `pemain_id` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `pelatih`;
CREATE TABLE `pelatih` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) DEFAULT NULL,
  `nama_pelatih` varchar(255) DEFAULT NULL,
  `lisensi` text,
  `jabatan` varchar(255) DEFAULT NULL,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tanggal_lahir` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `pemain`;
CREATE TABLE `pemain` (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `nama_pemain` varchar(255) DEFAULT NULL,
  `posisi` varchar(255) DEFAULT NULL,
  `tempat_lahir` varchar(255) DEFAULT NULL,
  `tanggal_lahir` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `perhitungan`;
CREATE TABLE `perhitungan` (
  `id` int NOT NULL AUTO_INCREMENT,
  `pemain_id` int DEFAULT NULL,
  `posisi` varchar(255) DEFAULT NULL,
  `nilai` decimal(15,3) DEFAULT NULL,
  `ranking` int DEFAULT NULL,
  `periode` bigint DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `pertandingan`;
CREATE TABLE `pertandingan` (
  `id` int NOT NULL AUTO_INCREMENT,
  `lineup_id` int DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `pertandingan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `statistik`;
CREATE TABLE `statistik` (
  `id` int NOT NULL AUTO_INCREMENT,
  `pemain_id` int DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `jumlah` int DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` text NOT NULL,
  `phone` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `alamat` text NOT NULL,
  `profile` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `kriteria` (`id`, `nama_kriteria`, `type`, `keep`, `defend`, `mid`, `attack`) VALUES
(2, 'Defending', 'benefit', 0.00, 0.20, 0.15, 0.05),
(3, 'Passing', 'benefit', 0.00, 0.15, 0.10, 0.10),
(5, 'Dribbling', 'benefit', 0.00, 0.10, 0.10, 0.10),
(6, 'Fisik', 'benefit', 0.00, 0.10, 0.15, 0.15),
(7, 'Speed', 'benefit', 0.00, 0.10, 0.10, 0.15),
(8, 'Fitness', 'benefit', 0.00, 0.10, 0.15, 0.05),
(9, 'Shooting', 'benefit', 0.00, 0.05, 0.10, 0.15),
(10, 'Adaptasi', 'cost', 0.00, 0.10, 0.10, 0.10),
(11, 'Mental', 'benefit', 0.00, 0.10, 0.05, 0.15);

INSERT INTO `latihan` (`id`, `kriteria_id`, `pelatih_id`, `nama_latihan`, `jam_selesai`, `jam_mulai`, `tanggal`) VALUES
(1, 2, 3, 'Latihan Press dan Cover', '2024-03-26 07:47:00', '2024-03-26 03:47:00', '2024-03-26 00:00:00'),
(2, 3, 3, 'Latihan Passing', '2024-03-28 16:20:00', '2024-03-28 16:19:00', '2024-03-28 00:00:00'),
(3, 5, 3, 'Latihan Dribling', '2024-03-28 18:28:00', '2024-03-28 17:26:00', '2024-03-28 00:00:00'),
(4, 6, 3, 'Latihan Fisik', '2024-03-28 18:28:00', '2024-03-28 17:26:00', '2024-03-28 00:00:00'),
(5, 7, 3, 'Latihan Speed', '2024-03-28 18:28:00', '2024-03-28 17:26:00', '2024-03-28 00:00:00'),
(6, 8, 3, 'Latihan Fitness', '2024-03-28 18:28:00', '2024-03-28 17:26:00', '2024-03-28 00:00:00'),
(7, 9, 3, 'Latihan Shooting', '2024-03-28 18:28:00', '2024-03-28 17:26:00', '2024-03-28 00:00:00'),
(8, 10, 3, 'Latihan Adaptasi', '2024-03-28 18:28:00', '2024-03-28 17:26:00', '2024-03-28 00:00:00'),
(9, 11, 3, 'Latihan Mental', '2024-03-28 18:28:00', '2024-03-28 17:26:00', '2024-03-28 00:00:00');

INSERT INTO `latihan_pemain` (`id`, `latihan_id`, `pemain_id`, `nilai`) VALUES
(1, 1, 1, 8.00),
(2, 1, 2, 6.00),
(3, 1, 3, 7.00),
(4, 1, 4, 8.00),
(5, 1, 5, 6.00),
(6, 1, 6, 7.00),
(7, 1, 7, 9.00),
(8, 1, 8, 6.00),
(9, 1, 9, 6.00),
(10, 2, 1, 7.00),
(11, 2, 2, 6.00),
(12, 2, 3, 7.00),
(13, 2, 4, 8.00),
(14, 2, 5, 8.00),
(15, 2, 6, 7.00),
(16, 2, 7, 6.00),
(17, 2, 8, 6.00),
(18, 2, 9, 6.00),
(19, 8, 9, 10.00),
(20, 8, 8, 3.00),
(21, 8, 7, 6.00),
(22, 8, 6, 7.00),
(23, 8, 5, 6.00),
(24, 8, 4, 8.00),
(25, 8, 3, 7.00),
(26, 8, 2, 5.00),
(27, 8, 1, 8.00),
(28, 3, 9, 7.00),
(29, 3, 8, 7.00),
(30, 3, 7, 7.00),
(31, 3, 6, 7.00),
(32, 3, 5, 7.00),
(33, 3, 4, 7.00),
(34, 3, 3, 7.00),
(35, 3, 2, 7.00),
(36, 3, 1, 7.00),
(37, 4, 9, 8.00),
(38, 4, 8, 7.00),
(39, 4, 7, 7.00),
(40, 4, 6, 8.00),
(41, 4, 5, 7.00),
(42, 4, 4, 7.00),
(43, 4, 3, 8.00),
(44, 4, 2, 7.00),
(45, 4, 1, 8.00),
(46, 6, 9, 6.00),
(47, 6, 8, 7.00),
(48, 6, 7, 6.00),
(49, 6, 6, 7.00),
(50, 6, 5, 6.00),
(51, 6, 4, 7.00),
(52, 6, 3, 6.00),
(53, 6, 2, 8.00),
(54, 6, 1, 6.00),
(55, 9, 9, 8.00),
(56, 9, 8, 8.00),
(57, 9, 7, 8.00),
(58, 9, 6, 8.00),
(59, 9, 5, 8.00),
(60, 9, 4, 8.00),
(61, 9, 3, 8.00),
(62, 9, 2, 8.00),
(63, 9, 1, 8.00),
(64, 7, 9, 8.00),
(65, 7, 8, 7.00),
(66, 7, 7, 8.00),
(67, 7, 6, 7.00),
(68, 7, 5, 9.00),
(69, 7, 4, 6.00),
(70, 7, 3, 5.00),
(71, 7, 2, 7.00),
(72, 7, 1, 5.00),
(73, 5, 9, 7.00),
(74, 5, 8, 7.00),
(75, 5, 7, 7.00),
(76, 5, 6, 9.00),
(77, 5, 5, 8.00),
(78, 5, 4, 8.00),
(79, 5, 3, 8.00),
(80, 5, 2, 8.00),
(81, 5, 1, 6.00);

INSERT INTO `lineup` (`id`, `nama_lineup`, `periode_id`, `created_at`) VALUES
(2, 'Line Up Kemenangan', 240329113030, '2024-03-29 12:55:38'),
(3, 'Line Up Dark System', 240329044046, '2024-03-29 16:41:18');

INSERT INTO `lineup_pemain` (`id`, `lineup_id`, `pemain_id`) VALUES
(1, 2, 2),
(2, 2, 5),
(3, 2, 8),
(4, 2, 6),
(5, 2, 3),
(6, 2, 4),
(7, 2, 7),
(8, 2, 1),
(9, 3, 2),
(10, 3, 5),
(11, 3, 8),
(12, 3, 6),
(13, 3, 3),
(14, 3, 9),
(15, 3, 4),
(16, 3, 7),
(17, 3, 1);

INSERT INTO `pelatih` (`id`, `user_id`, `nama_pelatih`, `lisensi`, `jabatan`, `tempat_lahir`, `tanggal_lahir`) VALUES
(3, '10', 'Pak Jojo Kusumo', '20240324103415.png', 'Pelatih Utama', 'Dusuntegenan', '2024-03-30 00:00:00'),
(4, '21', 'Pak Jojo Jeje', '20240330101624.jpg', 'Pelatih Pendamping', 'Denpasar', '2024-03-21 00:00:00');

INSERT INTO `pemain` (`id`, `user_id`, `nama_pemain`, `posisi`, `tempat_lahir`, `tanggal_lahir`) VALUES
(1, 11, 'Andika Ary', 'bek', 'Denpasar', '2024-03-23 00:00:00'),
(2, 12, 'Andika Ary 2', 'penyerang', 'Denpasar', '2024-03-28 00:00:00'),
(3, 13, 'Andika Ary 3', 'gelandang', 'Denpasar', '2024-03-27 00:00:00'),
(4, 14, 'Andika Ary 4', 'bek', 'Denpasar', '2024-03-23 00:00:00'),
(5, 15, 'Andika Ary 5', 'penyerang', 'Denpasar', '2024-03-28 00:00:00'),
(6, 16, 'Andika Ary 6', 'gelandang', 'Denpasar', '2024-03-27 00:00:00'),
(7, 17, 'Andika Ary 7', 'bek', 'Denpasar', '2024-03-23 00:00:00'),
(8, 18, 'Andika Ary 8', 'penyerang', 'Denpasar', '2024-03-28 00:00:00'),
(9, 19, 'Andika Ary 9', 'gelandang', 'Denpasar', '2024-03-27 00:00:00'),
(10, 20, 'Pak Coba Coba', 'penyerang', 'Denpasar', '2024-03-27 00:00:00');

INSERT INTO `perhitungan` (`id`, `pemain_id`, `posisi`, `nilai`, `ranking`, `periode`, `created_at`) VALUES
(1, 2, 'penyerang', 0.733, 1, 240329113030, '2024-03-29 11:30:30'),
(2, 5, 'penyerang', 0.733, 2, 240329113030, '2024-03-29 11:30:30'),
(3, 8, 'penyerang', 0.644, 3, 240329113030, '2024-03-29 11:30:30'),
(4, 6, 'gelandang', 0.821, 1, 240329113030, '2024-03-29 11:30:30'),
(5, 3, 'gelandang', 0.733, 2, 240329113030, '2024-03-29 11:30:30'),
(6, 9, 'gelandang', 0.644, 3, 240329113030, '2024-03-29 11:30:30'),
(7, 4, 'bek', 0.733, 1, 240329113030, '2024-03-29 11:30:30'),
(8, 7, 'bek', 0.644, 2, 240329113030, '2024-03-29 11:30:30'),
(9, 1, 'bek', 0.557, 3, 240329113030, '2024-03-29 11:30:30'),
(10, 2, 'penyerang', 0.733, 1, 240329044046, '2024-03-29 16:40:46'),
(11, 5, 'penyerang', 0.733, 2, 240329044046, '2024-03-29 16:40:46'),
(12, 8, 'penyerang', 0.644, 3, 240329044046, '2024-03-29 16:40:46'),
(13, 6, 'gelandang', 0.821, 1, 240329044046, '2024-03-29 16:40:46'),
(14, 3, 'gelandang', 0.733, 2, 240329044046, '2024-03-29 16:40:46'),
(15, 9, 'gelandang', 0.644, 3, 240329044046, '2024-03-29 16:40:46'),
(16, 4, 'bek', 0.733, 1, 240329044046, '2024-03-29 16:40:46'),
(17, 7, 'bek', 0.644, 2, 240329044046, '2024-03-29 16:40:46'),
(18, 1, 'bek', 0.557, 3, 240329044046, '2024-03-29 16:40:46');

INSERT INTO `pertandingan` (`id`, `lineup_id`, `status`, `tanggal`, `pertandingan`) VALUES
(1, 2, 'belum_bertanding', '2024-03-27 00:00:00', 'Pertandingan melawan Manchester United'),
(2, 3, 'belum_bertanding', '2024-04-10 00:00:00', 'Pertandingan melawan Real Madrid'),
(3, 2, 'belum_bertanding', '2024-04-17 00:00:00', 'Pertandingan melawan AC Milan'),
(4, 2, 'belum_bertanding', '2024-05-01 00:00:00', 'Pertandingan melawan Arema FC'),
(5, 2, 'belum_bertanding', '2024-06-11 00:00:00', 'Pertandingan melawan Manchester City'),
(6, 2, 'belum_bertanding', '2024-06-06 00:00:00', 'Pertandingan melawan Manchester Lampung');

INSERT INTO `statistik` (`id`, `pemain_id`, `type`, `jumlah`, `tanggal`) VALUES
(1, 1, 'goal', 8, '2024-03-29 00:00:00'),
(2, 1, 'assist', 10, '2024-03-29 00:00:00'),
(3, 1, 'yellow_card', 4, '2024-03-29 00:00:00'),
(4, 1, 'start', 1, '2024-03-29 00:00:00'),
(5, 1, 'red_card', 7, '2024-03-29 00:00:00');

INSERT INTO `users` (`id`, `username`, `email`, `password`, `phone`, `role`, `alamat`, `profile`) VALUES
(1, 'Admin', 'admin@gmail.com', 'cbfdac6008f9cab4083784cbd1874f76618d2a97', '088987406311', 'ADMIN', 'Jalan Raya Semer', 'noimage.jpeg'),
(10, 'Pak Jojo Kusumo', 'jojo@gmail.com', 'cbfdac6008f9cab4083784cbd1874f76618d2a97', '088987406311', 'PELATIH', 'Jalan Raya Semer', 'noimage.jpeg'),
(11, 'Andika Ary', 'andikaary99@gmail.com', 'cbfdac6008f9cab4083784cbd1874f76618d2a97', '088987406311', 'USER', 'Jalan Raya Semer', '20240330095439.jpg'),
(12, 'Andika Ary 2', 'andikaary2@gmail.com', 'cbfdac6008f9cab4083784cbd1874f76618d2a97', '088987406311', 'USER', 'Jalan Raya Semer', 'noimage.jpeg'),
(13, 'Andika Ary 3', 'andikaary3@gmail.com', 'cbfdac6008f9cab4083784cbd1874f76618d2a97', '088987406311', 'USER', 'Jalan Raya Semer', 'noimage.jpeg'),
(14, 'Andika Ary 4', 'andikaary4@gmail.com', 'cbfdac6008f9cab4083784cbd1874f76618d2a97', '088987406311', 'USER', 'Jalan Raya Semer', 'noimage.jpeg'),
(15, 'Andika Ary 5', 'andikaary5@gmail.com', 'cbfdac6008f9cab4083784cbd1874f76618d2a97', '088987406311', 'USER', 'Jalan Raya Semer', 'noimage.jpeg'),
(16, 'Andika Ary 6', 'andikaary6@gmail.com', 'cbfdac6008f9cab4083784cbd1874f76618d2a97', '088987406311', 'USER', 'Jalan Raya Semer', 'noimage.jpeg'),
(17, 'Andika Ary 7', 'andikaary7@gmail.com', 'cbfdac6008f9cab4083784cbd1874f76618d2a97', '088987406311', 'USER', 'Jalan Raya Semer', 'noimage.jpeg'),
(18, 'Andika Ary 8', 'andikaary8@gmail.com', 'cbfdac6008f9cab4083784cbd1874f76618d2a97', '088987406311', 'USER', 'Jalan Raya Semer', 'noimage.jpeg'),
(19, 'Andika Ary 9', 'andikaary9@gmail.com', 'cbfdac6008f9cab4083784cbd1874f76618d2a97', '088987406311', 'USER', 'Jalan Raya Semer', 'noimage.jpeg'),
(20, 'Pak Coba Coba', 'pemain123@gmail.com', 'cbfdac6008f9cab4083784cbd1874f76618d2a97', '088987406311', 'USER', 'awdawdawd', '20240330094720.png'),
(21, 'Pak Jojo Jeje', 'pelatih123@gmail.com', 'cbfdac6008f9cab4083784cbd1874f76618d2a97', '088987406311', 'PELATIH', 'adwawd', '20240330102452.png');



/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;