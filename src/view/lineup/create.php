<?php
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PELATIH', 'USER']);
    $breadcumb = 'lineup';
    $data = getDataTable($conn,'lineup');
    $kriterias = getDataTable($conn,'kriteria');
    $latihans = getDataJoinMore($conn, 'latihan', 'kriteria', 'kriteria_id', 'pelatih', 'pelatih_id');
    $posisi = getPosisi();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

	<?php require('../partials/links.php'); ?>
     
  </head>

<body class="hold-transition dark-skin sidebar-mini theme-danger fixed">
	
<div class="wrapper">
	<div id="loader"></div>
	
	<?php require('../partials/nav.php'); ?>
	<?php require('../partials/side.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title">Line Up</h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page">Line Up</li>
								<li class="breadcrumb-item active" aria-current="page">Generate Line Up</li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-xl-12 col-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <div class="col-12 d-flex justify-content-between">
                                    <h3 class="box-title">Pilih Nilai dari Latihan sesuai Kriteria</h3>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            
                            <form class="form" method="POST" action="../../proccess/lineup/proccess_store.php" enctype="multipart/form-data">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <table class="table table-hover">
                                                    <tr>
                                                        <th>Kriteria</th>
                                                        <th>Bobot</th>
                                                        <th>Latihan</th>
                                                    </tr>
                                                    <?php
                                                        while ($kriteria = $kriterias->fetch_assoc()) {
                                                    ?>
                                                        <tr>
                                                            <td><?= $kriteria['nama_kriteria'] ?></td>
                                                            <td>
                                                                <?php 
                                                                    foreach ($posisi as $key => $value) {
                                                                        $bobot = $key == 'penyerang' ? $kriteria['attack'] : (
                                                                                    $key == 'gelandang' ? $kriteria['mid'] : (
                                                                                        $key == 'bek' ? $kriteria['defend'] : $kriteria['keep']
                                                                                        )
                                                                                    );
                                                                        $persentase_bobot = $bobot * 100;
                                                                ?>
                                                                    <?= $value ?> = <?= $persentase_bobot ?>% <br>
                                                                <?php } ?>
                                                            </td>
                                                            <td width="40%">
                                                                <div class="form-group">
                                                                    <input type="hidden" name="kriteria_id[]" value="<?= $kriteria['id'] ?>">
                                                                    <select name="latihan[]" class="form-control" required>
                                                                        <option value="">Pilih Latihan</option>
    
                                                                        <?php
                                                                            while ($latihan = $latihans->fetch_assoc()) {
                                                                                if ($kriteria['id'] == $latihan['kriteria_id']) {
                                                                        ?>
                                                                                    <option value="<?= $latihan['latihan_id'] ?>"><?= $latihan['nama_latihan'] ?> | <?= date('d M Y', strtotime($latihan['tanggal'])) ?></option>
                                                                        <?php 
                                                                                }
                                                                            } mysqli_data_seek($latihans, 0);
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <a href="index.php" type="button" class="btn btn-rounded btn-warning btn-outline mr-1">
                                        <i class="ti-arrow-left"></i> Kembali
                                    </a>
                                    <button type="submit" class="btn btn-rounded btn-primary btn-outline">
                                        <i class="ti-save-alt"></i> Lanjut
                                    </button>
                                </div>  
                            </form>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
			</div>
		</section>
		<!-- /.content -->
	  </div>
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right d-none d-sm-inline-block">
    </div>
	  &copy; 2024 <a href="">Sistem Pendukung Keputusan</a>. Line Up Sepak Bola.
  </footer>

  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
	
	
	
	<!-- Vendor JS -->
	<?php require('../partials/scripts.php'); ?>
	
</body>
</html>
