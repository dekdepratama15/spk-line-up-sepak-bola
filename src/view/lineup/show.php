<?php
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PELATIH', 'USER']);
    $breadcumb = 'lineup';
    $data = getDataDetailForeignOrder($conn, 'lineup_pemain', $_GET['id'], 'lineup_id', 'id', 'ASC');

    $lineupData = getDataDetail($conn, 'lineup', $_GET['id']);
    $lineup = $lineupData->fetch_assoc();
    $perhitunganData = getDataDetailForeignOrder($conn, 'perhitungan', $lineup['periode_id'], 'periode', 'id', 'ASC');
    $perhitunganArray = getDataToArray($perhitunganData, 'pemain_id');
    
    $pemainData = getDataJoin($conn, 'pemain', 'users', 'user_id');
    $pemainArray = getDataToArray($pemainData, 'pemain_id');
    $posisi = getPosisi();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

	<?php require('../partials/links.php'); ?>
     
  </head>

<body class="hold-transition dark-skin sidebar-mini theme-danger fixed">
	
<div class="wrapper">
	<div id="loader"></div>
	
	<?php require('../partials/nav.php'); ?>
	<?php require('../partials/side.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title">Line Up</h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page">Line Up</li>
								<li class="breadcrumb-item active" aria-current="page">Lihat Line Up</li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-xl-12 col-12">
                        <div class="box">
                            <div class="box-header with-border">
                                <div class="col-12 d-flex justify-content-between">
                                    <h3 class="box-title">Detail Line Up</h3>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            
                            <form class="form" method="POST" action="" enctype="multipart/form-data">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Nama Line Up</label>
                                                <input type="text" class="form-control" placeholder="Nama Line Up" name="nama" value="<?= $lineup['nama_lineup'] ?>" readonly>
                                            </div>
                                        </div>
                                        <?php
                                            foreach ($posisi as $key => $value) {
                                        ?>
                                            <div class="col-md-6">
                                                <h5 class="text-warning"><?= $value ?></h5>
                                                <table class="table table-hover">
                                                    <tr>
                                                        <th>Profile</th>
                                                        <th>Nama Pemain</th>
                                                        <th>Nilai</th>
                                                        <th>Ranking</th>
                                                    </tr>
                                                    <?php
                                                        while ($perhitungan = $data->fetch_assoc()) {
                                                            if (isset($pemainArray[$perhitungan['pemain_id']]) && !empty($pemainArray[$perhitungan['pemain_id']])) {
                                                                $pemain = $pemainArray[$perhitungan['pemain_id']][0];
                                                                $nilai = $perhitunganArray[$perhitungan['pemain_id']][0];

                                                                if ($key == $pemain['posisi']) {
                                                    ?>
                                                                    <tr>
                                                                        <td>
                                                                            <img src="../../../upload/<?= $pemain['profile'] ?>" alt="" style="max-width: 100px;">
                                                                        </td>
                                                                        <td><?= $pemain['nama_pemain'] ?></td>
                                                                        <td><?= $nilai['nilai'] ?></td>
                                                                        <td><?= $nilai['ranking'] ?></td>
                                                                    </tr>
                                                    <?php       }
                                                            }
                                                        } mysqli_data_seek($data, 0); 
                                                    ?>
                                                </table>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <a href="index.php" type="button" class="btn btn-rounded btn-warning btn-outline mr-1">
                                        <i class="ti-arrow-left"></i> Kembali
                                    </a>
                                </div>  
                            </form>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
			</div>
		</section>
		<!-- /.content -->
	  </div>
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right d-none d-sm-inline-block">
    </div>
	  &copy; 2024 <a href="">Sistem Pendukung Keputusan</a>. Line Up Sepak Bola.
  </footer>

  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
	
	
	
	<!-- Vendor JS -->
	<?php require('../partials/scripts.php'); ?>
	
</body>
</html>
