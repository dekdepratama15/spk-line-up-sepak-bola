<?php
    include "../config/connection.php";
    include "../config/function.php";
    validSession(['ADMIN', 'PELATIH', 'USER']);
    $breadcumb = 'dashboard';
    $data = getDataTableWhereLike($conn, 'pertandingan', 'tanggal', 'LIKE', date('Y').'%');
    $pertandingan = getDataToArray($data);
	
	$group_pertandingan = [];
	foreach ($pertandingan as $key => $value) {
		$group_pertandingan[date('Y-m', strtotime($value['tanggal']))][] = $value;
	}
	$chart_data_pertandingan = [];
	$chart_label_pertandingan = [];
	foreach ($group_pertandingan as $key => $value) {
		$chart_data_pertandingan[] = count($value);
		$chart_label_pertandingan[] = date('M', strtotime($key));
	}

	$chart_data_statistik = [];
	$chart_label_statistik = [];
	if ($_SESSION['user']['role'] == 'USER') {
		$datapemain = getDataTableWhereLike($conn, 'pemain', 'user_id', '=', $_SESSION['user']['id']);
		$pemain = $datapemain->fetch_assoc();
		$datastatistik = getDataTableWhereLike($conn, 'statistik', 'pemain_id', '=', $pemain['id']);
		$statistik = getDataToArray($datastatistik);

		$group_statistik = [];
		foreach ($statistik as $key => $value) {
			$group_statistik[$value['type']][] = $value; 
		}
		$type_statistik = getTypeStatistik();
		foreach ($type_statistik as $key => $value) {
			$chart_label_statistik[] = $value;
			$total = 0;
			if (isset($group_statistik[$key]) && !empty($group_statistik[$key])) {
				foreach ($group_statistik[$key] as $key => $value) {
					$total += $value['jumlah'];
				}
			}
			$chart_data_statistik[] = $total;
		}
	}

	$total_pemain = getDataTable($conn, 'pemain')->num_rows;
	$total_pelatih = getDataTable($conn, 'pelatih')->num_rows;
	$total_lineup = getDataTable($conn, 'lineup')->num_rows;
	$total_kriteria = getDataTable($conn, 'kriteria')->num_rows;
	
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

	<?php require('partials/links.php'); ?>
     
  </head>

<body class="hold-transition dark-skin sidebar-mini theme-danger fixed">
	
<div class="wrapper">
	<div id="loader"></div>
	
	<?php require('partials/nav.php'); ?>
	<?php require('partials/side.php'); ?>
  

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-xl-8 col-12">
					<div class="box bg-primary">
						<div class="box-body d-flex px-0">
							<div class="flex-grow-1 p-30 flex-grow-1 bg-img dask-bg bg-none-md" style="background-position: right bottom; background-size: auto 100%; background-image: url(../../assets/images/svg-icon/color-svg/custom-1.svg)">
								<div class="row">
									<div class="col-12 col-xl-7">
										<h2>Selamat Datang, <strong><?= $_SESSION['user']['username'] ?>!</strong></h2>

										<p class="text-white my-10 font-size-16">
											Kamu adalah <strong class="text-warning"><?= ucfirst($_SESSION['user']['role']) ?>!</strong>
										</p>
									</div>
									<div class="col-12 col-xl-5"></div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<?php if ($_SESSION['user']['role'] == 'USER') { ?>
							<div class="col-xl-12 col-12">
								<div class="box">
									<div class="box-body">
										<h4 class="box-title">Statistik Pemain <?= date('Y') ?></h4>
										<div>
											<canvas id="radar-chart2" height="200"></canvas>
										</div>
									</div>
								</div>
							</div>
						<?php } else { ?>
							<div class="col-xl-12 col-12">
								<div class="box">
									<div class="box-body">
										<h4 class="box-title">Pertandingan Tahun <?= date('Y') ?></h4>
										<div>
											<canvas id="chLine"></canvas>
										</div>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>					
				</div>
				<div class="col-xl-4 col-12">
					<a href="pemain/index.php" class="box bg-danger bg-hover-danger pull-up">
						<div class="box-body">
							<div class="d-flex align-items-center">
								<div class="w-80 h-80 l-h-100 rounded-circle b-1 border-white text-center">
								<i class="icon-User font-size-40"><span class="path1"></span><span class="path2"></span></i>
								</div>
								<div class="ml-10">
									<h4 class="text-white mb-0"><?= $total_pemain ?></h4>
									<h5 class="text-white-50 mb-0">Total Pemain</h5>
								</div>
							</div>							
						</div>
					</a>
					<a href="pelatih/index.php" class="box bg-success bg-hover-success pull-up">
						<div class="box-body">
							<div class="d-flex align-items-center">
								<div class="w-80 h-80 l-h-100 rounded-circle b-1 border-white text-center">
								<i class="icon-User font-size-40"><span class="path1"></span><span class="path2"></span></i>
								</div>
								<div class="ml-10">
									<h4 class="text-white mb-0"><?= $total_pelatih ?></h4>
									<h5 class="text-white-50 mb-0">Total Pelatih</h5>
								</div>
							</div>							
						</div>
					</a>
					<a href="lineup/index.php" class="box bg-warning bg-hover-warning pull-up">
						<div class="box-body">
							<div class="d-flex align-items-center">
								<div class="w-80 h-80 l-h-100 rounded-circle b-1 border-white text-center">
									<i class="icon-Settings font-size-40"><span class="path1"></span><span class="path2"></span></i>
								</div>
								<div class="ml-10">
									<h4 class="text-white mb-0"><?= $total_lineup ?></h4>
									<h5 class="text-white-50 mb-0">Total Line Up</h5>
								</div>
							</div>							
						</div>
					</a>
					<a href="kriteria/index.php" class="box bg-primary bg-hover-primary pull-up">
						<div class="box-body">
							<div class="d-flex align-items-center">
								<div class="w-80 h-80 l-h-100 rounded-circle b-1 border-white text-center">
									<i class="icon-Brush font-size-40"><span class="path1"></span><span class="path2"></span></i>
								</div>
								<div class="ml-10">
									<h4 class="text-white mb-0"><?= $total_kriteria ?></h4>
									<h5 class="text-white-50 mb-0">Total Kriteria</h5>
								</div>
							</div>							
						</div>
					</a>
				</div>
				
			</div>
		</section>
		<!-- /.content -->
	  </div>
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right d-none d-sm-inline-block">
    </div>
	  &copy; 2024 <a href="">Sistem Pendukung Keputusan</a>. Line Up Sepak Bola.
  </footer>

  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
	
	
	
	<!-- Vendor JS -->
	<?php require('partials/scripts.php'); ?>
	<script>
		$(document).ready(function () {
			var data_chart = <?= json_encode($chart_data_pertandingan) ?>;
			var label_chart = <?= json_encode($chart_label_pertandingan) ?>;
			var data_statistik = <?= json_encode($chart_data_statistik) ?>;
			var label_statistik = <?= json_encode($chart_label_statistik) ?>;
			/* large line chart */
			var chLine = document.getElementById("chLine");
			var chartData = {
			labels: label_chart,
			datasets: [
			{
				data: data_chart,
				// backgroundColor: colors[3],
				borderColor: '#ff562f',
				borderWidth: 4,
				pointBackgroundColor: '#ff562f'
			}]
			};
			if (chLine) {
			new Chart(chLine, {
			type: 'line',
			data: chartData,
			options: {
				scales: {
				yAxes: [{
					ticks: {
					beginAtZero: false
					}
				}]
				},
				legend: {
				display: false
				},
				responsive: true
			}
			});
			}


			var ctx3 = document.getElementById("radar-chart2").getContext("2d");
		var data3 = {
			labels: label_statistik,
			datasets: [
				{
					label: "Statistik Pemain",
					backgroundColor: "#ff562f82",
					borderColor: "#ff562f",
					pointBackgroundColor: "#ff562f",
					pointBorderColor: "#fff",
					pointHoverBackgroundColor: "#fff",
					pointHoverBorderColor: "#ff562f",
					data: data_statistik
				}
			]
		};
		var radarChart = new Chart(ctx3, {
			type: "radar",
			data: data3,
			options: {
					scale: {
						ticks: {
							beginAtZero: true,
							fontFamily: "Nunito Sans",
							
						},
						gridLines: {
							color: "rgba(135,135,135,0)",
						},
						pointLabels:{
							fontFamily: "Nunito Sans",
							fontColor:"#878787"
						},
					},
					
					animation: {
						duration:	3000
					},
					responsive: true,
					legend: {
							labels: {
							fontFamily: "Nunito Sans",
							fontColor:"#878787"
							}
						},
						elements: {
							arc: {
								borderWidth: 0
							}
						},
						tooltip: {
						backgroundColor:'rgba(33,33,33,1)',
						cornerRadius:0,
						footerFontFamily:"'Nunito Sans'"
					}
			}
		});
		})
	</script>
</body>
</html>
