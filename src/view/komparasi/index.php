<?php
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PELATIH', 'USER']);
    $breadcumb = 'komparasi';
    $pemain = getDataTable($conn,'pemain');
	$chart_data_statistik1 = [];
	$chart_label_statistik1 = [];
	$chart_data_statistik2 = [];
	$chart_label_statistik2 = [];

	$pemain_1 = [];
	$pemain_2 = [];
    if (isset($_GET['pemain1']) && !empty($_GET['pemain1'])) {
		$datastatistik1 = getDataTableWhereLike($conn, 'statistik', 'pemain_id', '=', $_GET['pemain1']);
		$statistik1 = getDataToArray($datastatistik1);

		$group_statistik1 = [];
		foreach ($statistik1 as $key => $value) {
			$group_statistik1[$value['type']][] = $value; 
		}
		$type_statistik = getTypeStatistik();
		foreach ($type_statistik as $key => $value) {
			$chart_label_statistik1[] = $value;
			$total1 = 0;
			if (isset($group_statistik1[$key]) && !empty($group_statistik1[$key])) {
				foreach ($group_statistik1[$key] as $key => $value) {
					$total1 += $value['jumlah'];
				}
			}
			$chart_data_statistik1[] = $total1;
		}
		$pemain_1 = getDataJoinDetail($conn, 'pemain', 'users', 'user_id', $_GET['pemain1']);
		$data_pemain_1 = $pemain_1->fetch_assoc();
    }

    if (isset($_GET['pemain2']) && !empty($_GET['pemain2'])) {
        $datastatistik2 = getDataTableWhereLike($conn, 'statistik', 'pemain_id', '=', $_GET['pemain2']);
        $statistik2 = getDataToArray($datastatistik2);
    
        $group_statistik2 = [];
        foreach ($statistik2 as $key => $value) {
            $group_statistik2[$value['type']][] = $value; 
        }
        $type_statistik = getTypeStatistik();
        foreach ($type_statistik as $key => $value) {
            $chart_label_statistik2[] = $value;
            $total2 = 0;
            if (isset($group_statistik2[$key]) && !empty($group_statistik2[$key])) {
                foreach ($group_statistik2[$key] as $key => $value) {
                    $total2 += $value['jumlah'];
                }
            }
            $chart_data_statistik2[] = $total2;
        }
		$pemain_2 = getDataJoinDetail($conn, 'pemain', 'users', 'user_id', $_GET['pemain2']);
		$data_pemain_2 = $pemain_2->fetch_assoc();
    }

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

	<?php require('../partials/links.php'); ?>
     
  </head>

<body class="hold-transition dark-skin sidebar-mini theme-danger fixed">
	
<div class="wrapper">
	<div id="loader"></div>
	
	<?php require('../partials/nav.php'); ?>
	<?php require('../partials/side.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title">Komparasi</h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item active" aria-current="page">Komparasi</li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-xl-12 col-12">
                        <div class="box">
                            <div class="box-header with-border">
                            <div class="col-12 d-flex justify-content-between">
                                <h3 class="box-title">Komparasi Pemain</h3>
                            </div>
                            </div>
                            <!-- /.box-header -->
                            
                            <form class="form" method="GET" action="" enctype="multipart/form-data">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label>Pemain 1</label>
                                                <select name="pemain1" class="form-control" required>
                                                    <option value="">Pilih Pemain</option>
                                                    <?php foreach ($pemain as $key => $value) { ?>
                                                        <option value="<?= $value['id'] ?>" <?= isset($_GET['pemain1']) && $_GET['pemain1'] == $value['id'] ? 'selected' : ''?>><?= $value['nama_pemain'] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label>Pemain 2</label>
                                                <select name="pemain2" class="form-control" required>
                                                    <option value="">Pilih Pemain</option>
                                                    <?php foreach ($pemain as $key => $value) { ?>
                                                        <option value="<?= $value['id'] ?>" <?= isset($_GET['pemain1']) && $_GET['pemain2'] == $value['id'] ? 'selected' : ''?>><?= $value['nama_pemain'] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-2 d-flex justify-content-center align-items-center">
                                            <div class="form-group">
                                                <button class="btn btn-primary mt-3" type="submit">Komparasi</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <?php if (isset($_GET['pemain1']) && !empty($_GET['pemain1'])) { ?>
                                            <div class="col-xl-6 col-12">
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h4 class="box-title">Statistik Pemain</h4>
														<img src="../../../upload/<?= $data_pemain_1['profile'] ?>" alt="" style="position: absolute;max-width: 100px;max-height: 100px;left: 0;top: 64px;height: 100px;">
                                                        <div>
                                                            <canvas id="radar-chart2" height="200"></canvas>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <?php if (isset($_GET['pemain2']) && !empty($_GET['pemain2'])) { ?>
                                            <div class="col-xl-6 col-12">
                                                <div class="box">
                                                    <div class="box-body">
                                                        <h4 class="box-title">Statistik Pemain </h4>
														<img src="../../../upload/<?= $data_pemain_2['profile'] ?>" alt="" style="position: absolute;max-width: 100px;max-height: 100px;left: 0;top: 64px;height: 100px;">
                                                        <div>
                                                            <canvas id="radar-chart22" height="200"></canvas>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                </div>  
                            </form>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
			</div>
		</section>
		<!-- /.content -->
	  </div>
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right d-none d-sm-inline-block">
    </div>
	  &copy; 2024 <a href="">Sistem Pendukung Keputusan</a>. Line Up Sepak Bola.
  </footer>

  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
	
	
	
	<!-- Vendor JS -->
	<?php require('../partials/scripts.php'); ?>
	<script>
		$(document).ready(function () {
			var data_statistik1 = <?= json_encode($chart_data_statistik1) ?>;
			var label_statistik1 = <?= json_encode($chart_label_statistik1) ?>;

			var data_statistik2 = <?= json_encode($chart_data_statistik2) ?>;
			var label_statistik2 = <?= json_encode($chart_label_statistik2) ?>;


			var ctx3 = document.getElementById("radar-chart2").getContext("2d");
		var data3 = {
			labels: label_statistik1,
			datasets: [
				{
					label: "Statistik Pemain",
					backgroundColor: "#ff562f82",
					borderColor: "#ff562f",
					pointBackgroundColor: "#ff562f",
					pointBorderColor: "#fff",
					pointHoverBackgroundColor: "#fff",
					pointHoverBorderColor: "#ff562f",
					data: data_statistik1
				}
			]
		};
		var radarChart = new Chart(ctx3, {
			type: "radar",
			data: data3,
			options: {
					scale: {
						ticks: {
							beginAtZero: true,
							fontFamily: "Nunito Sans",
							
						},
						gridLines: {
							color: "rgba(135,135,135,0)",
						},
						pointLabels:{
							fontFamily: "Nunito Sans",
							fontColor:"#878787"
						},
					},
					
					animation: {
						duration:	3000
					},
					responsive: true,
					legend: {
							labels: {
							fontFamily: "Nunito Sans",
							fontColor:"#878787"
							}
						},
						elements: {
							arc: {
								borderWidth: 0
							}
						},
						tooltip: {
						backgroundColor:'rgba(33,33,33,1)',
						cornerRadius:0,
						footerFontFamily:"'Nunito Sans'"
					}
			}
		});



			var ctx33 = document.getElementById("radar-chart22").getContext("2d");
		var data33 = {
			labels: label_statistik2,
			datasets: [
				{
					label: "Statistik Pemain",
					backgroundColor: "#ff562f82",
					borderColor: "#ff562f",
					pointBackgroundColor: "#ff562f",
					pointBorderColor: "#fff",
					pointHoverBackgroundColor: "#fff",
					pointHoverBorderColor: "#ff562f",
					data: data_statistik2
				}
			]
		};
		var radarChart = new Chart(ctx33, {
			type: "radar",
			data: data33,
			options: {
					scale: {
						ticks: {
							beginAtZero: true,
							fontFamily: "Nunito Sans",
							
						},
						gridLines: {
							color: "rgba(135,135,135,0)",
						},
						pointLabels:{
							fontFamily: "Nunito Sans",
							fontColor:"#878787"
						},
					},
					
					animation: {
						duration:	3000
					},
					responsive: true,
					legend: {
							labels: {
							fontFamily: "Nunito Sans",
							fontColor:"#878787"
							}
						},
						elements: {
							arc: {
								borderWidth: 0
							}
						},
						tooltip: {
						backgroundColor:'rgba(33,33,33,1)',
						cornerRadius:0,
						footerFontFamily:"'Nunito Sans'"
					}
			}
		});
		})
	</script>
</body>
</html>
