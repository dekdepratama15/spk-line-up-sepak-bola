
<aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar position-relative">	
	  	<div class="multinav">
		  <div class="multinav-scroll" style="height: 100%;">	
			  <!-- sidebar menu-->
			  <ul class="sidebar-menu" data-widget="tree">	
				<li class="header">Dashboard</li>
				<li>
				  <a href="<?= $breadcumb == 'dashboard' ? '' : '../' ?>index.php">
					<i class="icon-Layout-4-blocks"><span class="path1"></span><span class="path2"></span></i>
					<span>Dashboard</span>
				  </a>
				</li>
				<li class="header">Pengguna</li>
				<li>
				  <a href="<?= $breadcumb == 'dashboard' ? '' : '../' ?>pelatih/index.php">
					<i class="icon-User"><span class="path1"></span><span class="path2"></span></i>
					<span>Pelatih</span>
				  </a>
				</li>
				<li>
				  <a href="<?= $breadcumb == 'dashboard' ? '' : '../' ?>pemain/index.php">
					<i class="icon-User"><span class="path1"></span><span class="path2"></span></i>
					<span>Pemain</span>
				  </a>
				</li>
				<li class="header">Komponen</li>
				<li>
				  <a href="<?= $breadcumb == 'dashboard' ? '' : '../' ?>kriteria/index.php">
					<i class="icon-Brush"><span class="path1"></span><span class="path2"></span></i>
					<span>Kriteria</span>
				  </a>
				</li>
				<li>
				  <a href="<?= $breadcumb == 'dashboard' ? '' : '../' ?>latihan/index.php">
					<i class="icon-Library"><span class="path1"></span><span class="path2"></span></i>
					<span>Latihan & Nilai</span>
				  </a>
				</li>
				<li>
				  <a href="<?= $breadcumb == 'dashboard' ? '' : '../' ?>pertandingan/index.php">
					<i class="icon-Clipboard-check"><span class="path1"></span><span class="path2"></span></i>
					<span>Pertandingan</span>
				  </a>
				</li>
				<li class="header">Perhitungan</li>
				<?php if ($_SESSION['user']['role'] == 'PELATIH') { ?>
					<li>
					  <a href="<?= $breadcumb == 'dashboard' ? '' : '../' ?>bobot/index.php">
						<i class="icon-Mailbox"><span class="path1"></span><span class="path2"></span></i>
						<span>Bobot</span>
					  </a>
					</li>
                <?php } ?>
				<li>
				  <a href="<?= $breadcumb == 'dashboard' ? '' : '../' ?>lineup/index.php">
					<i class="icon-Settings"><span class="path1"></span><span class="path2"></span></i>
					<span>Line Up</span>
				  </a>
				</li>
				<?php if ($_SESSION['user']['role'] == 'PELATIH') { ?>	
					<li>
						<a href="<?= $breadcumb == 'dashboard' ? '' : '../' ?>statistik/index.php">
							<i class="icon-Chart-line"><span class="path1"></span><span class="path2"></span></i>
							<span>Statistik</span>
						</a>
					</li>
                <?php } ?>	
				<li>
				  <a href="<?= $breadcumb == 'dashboard' ? '' : '../' ?>komparasi/index.php">
					<i class="icon-Scale"><span class="path1"></span><span class="path2"></span></i>
					<span>Komparasi</span>
				  </a>
				</li>	     
			  </ul>
		  </div>
		</div>
    </section>
  </aside>