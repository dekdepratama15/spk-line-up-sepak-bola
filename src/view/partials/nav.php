
<header class="main-header">
	<div class="d-flex align-items-center logo-box justify-content-start">
		<a href="#" class="waves-effect waves-light nav-link d-none d-md-inline-block mx-10 push-btn bg-transparent" data-toggle="push-menu" role="button">
			<span class="icon-Align-left"><span class="path1"></span><span class="path2"></span><span class="path3"></span></span>
		</a>	
		<!-- Logo -->
		<a href="index.html" class="logo">
		  <!-- logo-->
		  <div class="logo-lg">
			  <span class="light-logo"><img src="<?= $breadcumb == 'dashboard' ? '' : '../' ?>../../assets/images/logo/logo.png" alt="logo"></span>
			  <span class="dark-logo"><img src="<?= $breadcumb == 'dashboard' ? '' : '../' ?>../../assets/images/logo/logo.png" alt="logo" style="max-width: 90px"></span>
		  </div>
		</a>	
	</div>  
    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
	  <div class="app-menu">
		<ul class="header-megamenu nav">
			<li class="btn-group nav-item d-md-none">
				<a href="#" class="waves-effect waves-light nav-link push-btn" data-toggle="push-menu" role="button">
					<span class="icon-Align-left"><span class="path1"></span><span class="path2"></span><span class="path3"></span></span>
			    </a>
			</li>
		</ul> 
	  </div>
		
      <div class="navbar-custom-menu r-side">
        <ul class="nav navbar-nav">	
			<li class="btn-group nav-item d-lg-inline-flex d-none">
				<a href="#" data-provide="fullscreen" class="waves-effect waves-light nav-link full-screen" title="Full Screen">
					<i class="icon-Expand-arrows"><span class="path1"></span><span class="path2"></span></i>
			    </a>
			</li>	  
		  
	      <!-- User Account-->
          <li class="dropdown user user-menu">
            <a href="#" class="waves-effect waves-light dropdown-toggle" data-toggle="dropdown" title="User">
				<i class="icon-User"><span class="path1"></span><span class="path2"></span></i>
            </a>
            <ul class="dropdown-menu animated flipInX">
              <li class="user-body">
				<?php if ($_SESSION['user']['role'] != 'ADMIN') { 
					$role_user_login = $_SESSION['user']['role'] == 'PELATIH' ? 'pelatih' : 'pemain';
					if ($role_user_login == 'pemain') {
						$datapemainlogin = getDataTableWhereLike($conn, 'pemain', 'user_id', '=', $_SESSION['user']['id']);
						$pemainlogin = $datapemainlogin->fetch_assoc();
						$id_profile = $pemainlogin['id'];
					} else {
						$datapelatihlogin = getDataTableWhereLike($conn, 'pelatih', 'user_id', '=', $_SESSION['user']['id']);
						$pelatihlogin = $datapelatihlogin->fetch_assoc();
						$id_profile = $pelatihlogin['id'];
					}
				?>
					<a class="dropdown-item" href="<?= $breadcumb == 'dashboard' ? '' : '../' ?><?= $role_user_login ?>/edit.php?id=<?= $id_profile ?>&profile=true"><i class="ti-user text-muted mr-2"></i> Profile</a>
					<div class="dropdown-divider"></div>
				<?php } ?>
				 <a class="dropdown-item" href="<?= $breadcumb == 'dashboard' ? '' : '../' ?>../proccess/proccess_logout.php"><i class="ti-lock text-muted mr-2"></i> Logout</a>
              </li>
            </ul>
          </li>	
		  
			
        </ul>
      </div>
    </nav>
  </header>