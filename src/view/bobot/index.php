<?php
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PELATIH', 'USER']);
    $breadcumb = 'bobot';
    $data = getDataTable($conn, 'kriteria');
    $posisi = getPosisi();
    $type_kriteria = getTypeKriteria();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

	<?php require('../partials/links.php'); ?>
     
  </head>

<body class="hold-transition dark-skin sidebar-mini theme-danger fixed">
	
<div class="wrapper">
	<div id="loader"></div>
	
	<?php require('../partials/nav.php'); ?>
	<?php require('../partials/side.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title">Bobot</h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item active" aria-current="page">Bobot</li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-xl-12 col-12">
                        <?php 
                            if (isset($_SESSION['alert_bobot'])) {
                                $alert = $_SESSION['alert_bobot'];
                        ?>
                                <div class="alert alert-<?= $alert['icon'] ?>" role="alert">
                                    <?= $alert['message'] ?>
                                </div>
                        <?php
                                unset($_SESSION['alert_bobot']);
                            }
                        ?>
                        <div class="box">
                            <div class="box-header with-border">
                            <div class="col-12 d-flex justify-content-between">
                                <h3 class="box-title">Pembagian Bobot</h3>
                            </div>
                            </div>
                            <!-- /.box-header -->
                            
                            <form class="form" method="POST" action="../../proccess/bobot/proccess_store.php" enctype="multipart/form-data">
                                <div class="box-body">
                                    <div class="row">
                                        <?php if ($data->num_rows > 0) { ?>
                                            <?php
                                                foreach ($posisi as $key => $value) {
                                            ?>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <h5 class="text-warning"><?= $value ?></h5>
                                                        <table class="table table-hover">
                                                            <tr>
                                                                <th>Kriteria</th>
                                                                <th>Persentase Bobot</th>
                                                            </tr>
                                                            <?php
                                                                $total_persentase_bobot = 0;
                                                                while ($kriteria = $data->fetch_assoc()) {
                                                                    $bobot = $key == 'penyerang' ? $kriteria['attack'] : (
                                                                                $key == 'gelandang' ? $kriteria['mid'] : (
                                                                                    $key == 'bek' ? $kriteria['defend'] : $kriteria['keep']
                                                                        )
                                                                    );
                                                                    $persentase_bobot = $bobot * 100;
                                                                    $total_persentase_bobot += $persentase_bobot;
                                                            ?>
                                                                <tr>
                                                                    <td><?= $kriteria['nama_kriteria'] ?> (<?= $type_kriteria[$kriteria['type']] ?>)</td>
                                                                    <td width="40%">
                                                                    <div class="input-group mb-3">
                                                                        <input type="number" class="form-control input_bobot input_bobot_<?= $key ?>" data-posisi="<?= $key ?>" name="bobot_<?= $key ?>[]" placeholder="Persentase Bobot" value="<?= $persentase_bobot ?>">
                                                                        <div class="input-group-append">
                                                                            <span class="input-group-text"><i class="fa fa-percent"></i></span>
                                                                        </div>
                                                                    </div>
                                                                    </td>
                                                                </tr>
                                                            <?php } ?>
                                                            <tr>
                                                                <th class="text-center">Total Persentase</th>
                                                                <th>
                                                                    <span id="bobot_<?= $key ?>"><?= $total_persentase_bobot ?></span>%
                                                                    <input type="hidden" value="<?= $total_persentase_bobot ?>" class="persentase_bobot" name="persentase_bobot_<?= $key ?>" id="persentase_bobot_<?= $key ?>">
                                                                </th>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            <?php mysqli_data_seek($data, 0); } ?>
                                        <?php } else { ?>
                                            <div class="col-md-12 text-center">
                                                <h5>Mohon untuk menginputkan data kriteria lebih dahulu</h5>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <div class="col-lg-12 mb-5">
                                        <p class="text-primary"><i>Pastikan semua bobot mencapai 100% sebelum disimpan</i></p>
                                    </div>
                                    <a href="index.php" type="button" class="btn btn-rounded btn-warning btn-outline mr-1">
                                        <i class="ti-arrow-left"></i> Kembali
                                    </a>
                                    <?php if ($data->num_rows > 0) { ?>
                                        <button type="submit" class="btn btn-rounded btn-primary btn-outline" id="btn_submit_bobot">
                                            <i class="ti-save-alt"></i> Simpan
                                        </button>
                                    <?php } ?>
                                </div>  
                            </form>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
			</div>
		</section>
		<!-- /.content -->
	  </div>
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right d-none d-sm-inline-block">
    </div>
	  &copy; 2024 <a href="">Sistem Pendukung Keputusan</a>. Line Up Sepak Bola.
  </footer>

  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
	
	
	
	<!-- Vendor JS -->
	<?php require('../partials/scripts.php'); ?>
	<script>
        $(document).ready(function () {
            checkCanSave();
            $('.input_bobot').on('input', function () {
                var value = $(this).val();
                if (value != '') {
                    var total = 0;
                    var posisi = $(this).data('posisi');
                    $.each($('.input_bobot_' + posisi), function (key, val) {
                        total += parseInt($(val).val());
                    });
                    $('#bobot_' + posisi).html(total);
                    $('#persentase_bobot_' + posisi).val(total);
                    
                    checkCanSave();
                }
            });
        });

        function checkCanSave() {
            var cansave = true;
            $.each($('.persentase_bobot'), function (key, val) {
                if ($(val).val() != 100) {
                    cansave = false;
                }
            })

            if (cansave) {
                $('#btn_submit_bobot').removeAttr('disabled');
            } else {
                $('#btn_submit_bobot').attr('disabled', true);
            }
        }
    </script>
</body>
</html>
