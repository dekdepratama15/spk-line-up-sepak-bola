<?php
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PELATIH', 'USER']);
    $breadcumb = 'pelatih';
    $data = getDataJoinDetail($conn, 'pemain', 'users', 'user_id', $_GET['id']);
    $pemain = $data->fetch_assoc();
    $posisi = getPosisi();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

	<?php require('../partials/links.php'); ?>
     
  </head>

<body class="hold-transition dark-skin sidebar-mini theme-danger fixed">
	
<div class="wrapper">
	<div id="loader"></div>
	
	<?php require('../partials/nav.php'); ?>
	<?php require('../partials/side.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title">Pemain</h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item" aria-current="page">Pemain</li>
								<li class="breadcrumb-item active" aria-current="page">Ubah Pemain</li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-xl-12 col-12">
                        <div class="box">
                            <div class="box-header with-border">
                            <div class="col-12 d-flex justify-content-between">
                                <h3 class="box-title">Ubah Pemain</h3>
                            </div>
                            </div>
                            <!-- /.box-header -->
                            
                            <form class="form" method="POST" action="../../proccess/pemain/proccess_update.php?id=<?= $pemain['user_id'] ?>&profile=<?= $_GET['profile'] ?? false ?>" enctype="multipart/form-data">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Nama Pemain</label>
                                                <input type="text" class="form-control" placeholder="Nama Pemain" name="nama" value="<?= $pemain['nama_pemain'] ?>" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Posisi</label>
                                                <select name="posisi" class="form-control" required>
                                                    <option value="">Pilih Posisi</option>
                                                    <?php foreach ($posisi as $key => $value) { ?>
                                                        <option value="<?= $key ?>" <?= $pemain['posisi'] == $key  ? 'selected' : '' ?>><?= $value ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Foto Profile</label><br>
                                                <div class="input-group">
                                                    <input type="file" class="form-control" placeholder="Foto Profile" name="profile">
                                                    <a type="button" href="../../../upload/<?= $pemain['profile'] ?>" target="_blank" class="btn btn-sm btn-primary">Lihat Profile</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Username</label>
                                                <input type="text" class="form-control" placeholder="Username" name="username" value="<?= $pemain['username'] ?>" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>No. Telp</label>
                                                <input type="number" class="form-control" placeholder="No. Telp" name="telp" value="<?= $pemain['phone'] ?>" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Password</label>
                                                <input type="password" class="form-control" placeholder="Password" name="password" >
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Konfirmasi Password</label>
                                                <input type="password" class="form-control" placeholder="Konfirmasi Password" name="confirm_password" >
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Tempat Lahir</label>
                                                <input type="text" class="form-control" placeholder="Tempat Lahir" name="tempat_lahir" value="<?= $pemain['tempat_lahir'] ?>" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Tanggal Lahir</label>
                                                <input type="date" class="form-control" placeholder="Tanggal Lahir" name="tgl_lahir" value="<?= date('Y-m-d', strtotime($pemain['tanggal_lahir'])) ?>" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Alamat</label>
                                                <textarea class="form-control" placeholder="Alamat" name="alamat" readonly><?= $pemain['alamat'] ?></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <a href="index.php" type="button" class="btn btn-rounded btn-warning btn-outline mr-1">
                                        <i class="ti-arrow-left"></i> Kembali
                                    </a>
                                    <button type="submit" class="btn btn-rounded btn-primary btn-outline">
                                        <i class="ti-save-alt"></i> Simpan
                                    </button>
                                </div>  
                            </form>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
			</div>
		</section>
		<!-- /.content -->
	  </div>
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right d-none d-sm-inline-block">
    </div>
	  &copy; 2024 <a href="">Sistem Pendukung Keputusan</a>. Line Up Sepak Bola.
  </footer>

  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
	
	
	
	<!-- Vendor JS -->
	<?php require('../partials/scripts.php'); ?>
	
</body>
</html>
