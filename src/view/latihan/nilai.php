<?php
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PELATIH', 'USER']);
    $breadcumb = 'bobot';
    $cek = getDataDetailForeign($conn, 'latihan_pemain', $_GET['id'], 'latihan_id');
    if ($cek->num_rows > 0) {
        $data = getDataJoinMoreLeftForeign($conn, 'pemain', 'users', 'user_id', 'latihan_pemain', 'pemain_id', 'latihan_id', $_GET['id']);
    } else {
        $data = getDataJoin($conn, 'pemain', 'users', 'user_id');
    }
    $posisi = getPosisi();
    $result = getDataJoinMoreDetail($conn, 'latihan', 'kriteria', 'kriteria_id', 'pelatih', 'pelatih_id', $_GET['id']);
    $latihan = $result->fetch_assoc();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

	<?php require('../partials/links.php'); ?>
     
  </head>

<body class="hold-transition dark-skin sidebar-mini theme-danger fixed">
	
<div class="wrapper">
	<div id="loader"></div>
	
	<?php require('../partials/nav.php'); ?>
	<?php require('../partials/side.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title">Nilai Latihan</h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item active" aria-current="page">Nilai Latihan</li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-xl-12 col-12">
                        <?php 
                            if (isset($_SESSION['alert_nilai_latihan'])) {
                                $alert = $_SESSION['alert_nilai_latihan'];
                        ?>
                                <div class="alert alert-<?= $alert['icon'] ?>" role="alert">
                                    <?= $alert['message'] ?>
                                </div>
                        <?php
                                unset($_SESSION['alert_nilai_latihan']);
                            }
                        ?>
                        <div class="box">
                            <div class="box-header with-border">
                            <div class="col-12 d-flex justify-content-between">
                                <h3 class="box-title">Penilaian Latihan</h3>
                            </div>
                            </div>
                            <!-- /.box-header -->
                            
                            <form class="form" method="POST" action="../../proccess/latihan/proccess_store_nilai.php" enctype="multipart/form-data">
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Nama Latihan</label>
                                                <input type="text" class="form-control" placeholder="Nama Latihan" name="nama" value="<?= $latihan['nama_latihan'] ?>" readonly>
                                                <input type="hidden" name="latihan_id" value="<?= $latihan['latihan_id'] ?>" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Pelatih</label>
                                                <input type="text" class="form-control" placeholder="Pelatih" name="pelatih" value="<?= $latihan['nama_pelatih'] ?>" readonly>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Kriteria</label>
                                                <input type="text" class="form-control" placeholder="Kriteria" name="nama" value="<?= $latihan['nama_kriteria'] ?>" readonly>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <?php if ($data->num_rows > 0) { ?>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <table class="table table-hover">
                                                        <tr>
                                                            <th>Nama Pemain</th>
                                                            <th>Posisi</th>
                                                            <th>Nilai ( <span class="text-primary">Masukan nilai 1 - 10</span> )</th>
                                                        </tr>
                                                        <?php
                                                            $nilai_update = '';
                                                            while ($pemain = $data->fetch_assoc()) {
                                                                if (isset($pemain['nilai']) && !empty($pemain['nilai'])) {
                                                                    if ($nilai_update != '') {
                                                                        $nilai_update .= ',';
                                                                    }
                                                                    $nilai_update .= $pemain['pemain_id'];
                                                                }
                                                        ?>
                                                            <tr>
                                                                <td><?= $pemain['nama_pemain'] ?></td>
                                                                <td><?= $posisi[$pemain['posisi']] ?></td>
                                                                <td width="30%">
                                                                <div class="input-group mb-3">
                                                                    <input type="number" class="form-control input_nilai" name="<?= isset($pemain['nilai']) && !empty($pemain['nilai']) ? 'nilai_update[]' : 'nilai_create[]' ?>" placeholder="Nilai" value="<?= isset($pemain['nilai']) && !empty($pemain['nilai']) ? round($pemain['nilai']) : '' ?>" required>
                                                                </div>
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                        <input type="hidden" name="nilai_update_id" value="<?= $nilai_update ?>">
                                                    </table>
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                            <div class="col-md-12 text-center">
                                                <h5>Mohon untuk menginputkan data pemain lebih dahulu</h5>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
                                    <div class="col-lg-12 mb-5">
                                        <p class="text-primary"><i>Pastikan semua nilai sudah sesuai sebelum disimpan</i></p>
                                    </div>
                                    <a href="index.php" type="button" class="btn btn-rounded btn-warning btn-outline mr-1">
                                        <i class="ti-arrow-left"></i> Kembali
                                    </a>
                                    <?php if ($data->num_rows > 0) { ?>
                                        <button type="submit" class="btn btn-rounded btn-primary btn-outline" id="btn_submit_bobot">
                                            <i class="ti-save-alt"></i> Simpan
                                        </button>
                                    <?php } ?>
                                </div>  
                            </form>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
			</div>
		</section>
		<!-- /.content -->
	  </div>
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right d-none d-sm-inline-block">
    </div>
	  &copy; 2024 <a href="">Sistem Pendukung Keputusan</a>. Line Up Sepak Bola.
  </footer>

  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
	
	
	
	<!-- Vendor JS -->
	<?php require('../partials/scripts.php'); ?>
	<script>
        $(document).ready(function () {
            checkCanSave();
            $('.input_bobot').on('input', function () {
                var value = $(this).val();
                if (value != '') {
                    var total = 0;
                    var posisi = $(this).data('posisi');
                    $.each($('.input_bobot_' + posisi), function (key, val) {
                        total += parseInt($(val).val());
                    });
                    $('#bobot_' + posisi).html(total);
                    $('#persentase_bobot_' + posisi).val(total);
                    
                    checkCanSave();
                }
            });
        });

        function checkCanSave() {
            var cansave = true;
            $.each($('.persentase_bobot'), function (key, val) {
                if ($(val).val() != 100) {
                    cansave = false;
                }
            })

            if (cansave) {
                $('#btn_submit_bobot').removeAttr('disabled');
            } else {
                $('#btn_submit_bobot').attr('disabled', true);
            }
        }
    </script>
</body>
</html>
