<?php
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PELATIH', 'USER']);
    $breadcumb = 'pelatih';
    $data = getDataJoin($conn, 'pelatih', 'users', 'user_id')
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

	<?php require('../partials/links.php'); ?>
     
  </head>

<body class="hold-transition dark-skin sidebar-mini theme-danger fixed">
	
<div class="wrapper">
	<div id="loader"></div>
	
	<?php require('../partials/nav.php'); ?>
	<?php require('../partials/side.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
	  <div class="container-full">
		<div class="content-header">
			<div class="d-flex align-items-center">
				<div class="mr-auto">
					<h3 class="page-title">Pelatih</h3>
					<div class="d-inline-block align-items-center">
						<nav>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="#"><i class="mdi mdi-home-outline"></i></a></li>
								<li class="breadcrumb-item active" aria-current="page">Pelatih</li>
							</ol>
						</nav>
					</div>
				</div>
				
			</div>
		</div>
		<!-- Main content -->
		<section class="content">
			<div class="row">
				<div class="col-xl-12 col-12">
                        <?php 
                            if (isset($_SESSION['alert_pelatih'])) {
                                $alert = $_SESSION['alert_pelatih'];
                        ?>
                                <div class="alert alert-<?= $alert['icon'] ?>" role="alert">
                                    <?= $alert['message'] ?>
                                </div>
                        <?php
                                unset($_SESSION['alert_pelatih']);
                            }
                        ?>
                        <div class="box">
                            <div class="box-header with-border">
                            <div class="col-12 d-flex justify-content-between">
                                <h3 class="box-title">List Pelatih</h3>
                                <?php if ($_SESSION['user']['role'] == 'ADMIN') { ?>
                                    <a type="button" href="create.php" class="btn btn-sm btn-primary"> <i class="fa fa-plus"></i> Tambah</a>
                                <?php } ?>
                            </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">
                                <div class="table-responsive">
                                <table id="example123" class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th>Nama Pelatih</th>
                                            <th>TTL</th>
                                            <th>Jabatan</th>
                                            <th>Alamat</th>
                                            <th>Lisensi</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if ($data->num_rows > 0) { ?>
                                            <?php
                                                while ($value = $data->fetch_assoc()) {
                                            ?>
                                                <tr>
                                                    <td><?= $value['nama_pelatih'] ?></td>
                                                    <td><?= $value['tempat_lahir'] ?>, <?= date('d M Y', strtotime($value['tanggal_lahir'])) ?></td>
                                                    <td><?= $value['jabatan'] ?></td>
                                                    <td><?= $value['alamat'] ?></td>
                                                    <td>
                                                        <a type="button" href="../../../upload/<?= $value['lisensi'] ?>" target="_blank" class="btn btn-sm btn-info">Lihat Lisensi</a>
                                                    </td>
                                                    <td>
                                                        <a type="button" href="show.php?id=<?= $value['pelatih_id'] ?>" class="btn btn-sm btn-dark"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                        <?php if ($_SESSION['user']['role'] == 'ADMIN') { ?>
                                                            <a type="button" href="edit.php?id=<?= $value['pelatih_id'] ?>" class="btn btn-sm btn-danger"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                            <button type="button" class="btn btn-sm btn-warning btn-delete-data" data-id="<?= $value['pelatih_id'] ?>" data-user_id="<?= $value['user_id'] ?>" data-tabel="pelatih" data-redirect="pelatih"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <tr>
                                                <td colspan="6" class="text-center">Tidak ada data</td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <th>Nama Pelatih</th>
                                            <th>TTL</th>
                                            <th>Jabatan</th>
                                            <th>Alamat</th>
                                            <th>Lisensi</th>
                                            <th>Action</th>
                                        </tr>
                                    </tfoot>
                                </table>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
			</div>
		</section>
		<!-- /.content -->
	  </div>
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right d-none d-sm-inline-block">
    </div>
	  &copy; 2024 <a href="">Sistem Pendukung Keputusan</a>. Line Up Sepak Bola.
  </footer>

  
  <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
  
</div>
<!-- ./wrapper -->
	
	
	
	<!-- Vendor JS -->
	<?php require('../partials/scripts.php'); ?>
	
</body>
</html>
