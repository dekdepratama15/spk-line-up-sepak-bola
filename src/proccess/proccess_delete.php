<?php 
    include "../config/connection.php";
    include "../config/function.php";
    validSession(['ADMIN', 'PELATIH', 'USER']);
    if (isset($_GET['id'])) {
        $deleted = $conn->query('DELETE FROM '.$_GET['tabel'].' WHERE id='.$_GET['id']);
        if ($_GET['user_id'] != 0) {
            $conn->query('DELETE FROM users WHERE id='.$_GET['user_id']);
        }
        if ($deleted) {
            $response['error']   = false;
            $response['icon']    = 'success';
            $response['message'] = 'Berhasil menghapus data';
        } else {
            $response['error']   = true;
            $response['icon']    = 'error';
            $response['message'] = 'Gagal menghapus data';
        }
        
    } else {
        $response['error']   = true;
        $response['icon']    = 'warning';
        $response['message'] = 'ID tidak ditemukan';
    }
    $_SESSION['alert_'.$_GET['redirect']] = $response;
    
    header('location: ../view/'.$_GET['redirect'].'/index.php');
?>