<?php
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PELATIH', 'USER']);
    // var_dump($_POST, $_FILES);die();
    if (isset($_POST)) {
        
        extract($_POST);
        try {
            $conn->autocommit(FALSE);
            $conn->query("INSERT INTO kriteria VALUES(NULL, '".htmlspecialchars($nama_kriteria)."', '".htmlspecialchars($type_kriteria)."', 0.00, 0.00, 0.00, 0.00)");
            $insert = true;
            $conn->commit();
        } catch (Exception $e) {
            $insert = false;
            $conn->rollback();
            $conn->close();
        }
        if ($insert) {
            $response['error']   = false;
            $response['icon']    = 'success';
            $response['message'] = 'Berhasil menambahkan data';
        } else {
            $response['error']   = true;
            $response['icon']    = 'danger';
            $response['message'] = 'Gagal menambahkan data';
        }
    } else {
        $response['error']   = true;
        $response['icon']    = 'warning';
        $response['message'] = 'Harap memasukan data lewat form';
    }
    $_SESSION['alert_kriteria'] = $response;
    
    header('location: ../../view/kriteria/index.php');
    exit(); 
?>