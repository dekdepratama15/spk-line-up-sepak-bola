<?php
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PELATIH', 'USER']);
    // var_dump($_POST, $_FILES);die();
    if (isset($_POST)) {
        
        extract($_POST);
        try {
            $conn->autocommit(FALSE);
            $tanggal_mulai = $tanggal.' '.$jam_mulai.':00';
            $tanggal_selesai = $tanggal.' '.$jam_selesai.':00';
            $conn->query("INSERT INTO latihan VALUES(NULL,".$kriteria.",".$pelatih.", '".htmlspecialchars($nama)."', '".$tanggal_selesai."', '".$tanggal_mulai."', '".$tanggal."')");
            $insert = true;
            $conn->commit();
        } catch (Exception $e) {
            var_dump($e);die();
            $insert = false;
            $conn->rollback();
            $conn->close();
        }
        if ($insert) {
            $response['error']   = false;
            $response['icon']    = 'success';
            $response['message'] = 'Berhasil menambahkan data';
        } else {
            $response['error']   = true;
            $response['icon']    = 'danger';
            $response['message'] = 'Gagal menambahkan data';
        }
    } else {
        $response['error']   = true;
        $response['icon']    = 'warning';
        $response['message'] = 'Harap memasukan data lewat form';
    }
    $_SESSION['alert_latihan'] = $response;
    
    header('location: ../../view/latihan/index.php');
    exit(); 
?>