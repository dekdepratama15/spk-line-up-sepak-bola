<?php
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PELATIH', 'USER']);
    if (isset($_POST)) {
        
        extract($_POST);

            
        try {
            $conn->autocommit(FALSE);
            $tanggal_mulai = $tanggal.' '.$jam_mulai.':00';
            $tanggal_selesai = $tanggal.' '.$jam_selesai.':00';
            $conn->query("UPDATE latihan SET nama_latihan='".htmlspecialchars($nama)."', kriteria_id=".$kriteria.", pelatih_id=".$pelatih.", tanggal='".$tanggal."', jam_mulai='".$tanggal_mulai."', jam_selesai='".$tanggal_selesai."' WHERE id=".$_GET['id']);
            $update = true;
            $conn->commit();
        } catch (Exception $e) {
            var_dump($e);die();
            $update = false;
            $conn->rollback();
            $conn->close();
        }
        if ($update) {
            $response['error']   = false;
            $response['icon']    = 'success';
            $response['message'] = 'Berhasil mengubah data';
        } else {
            $response['error']   = true;
            $response['icon']    = 'danger';
            $response['message'] = 'Gagal mengubah data';
        }
    } else {
        $response['error']   = true;
        $response['icon']    = 'warning';
        $response['message'] = 'Harap memasukan data lewat form';
    }
    $_SESSION['alert_latihan'] = $response;
    
    header('location: ../../view/latihan/index.php');
    exit(); 
?>