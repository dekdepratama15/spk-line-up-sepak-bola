<?php
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PELATIH', 'USER']);
    $data = getDataJoin($conn, 'pemain', 'users', 'user_id');
    if (isset($_POST)) {
        
        extract($_POST);
        try {
            $conn->autocommit(FALSE);
            if (isset($nilai_create) && !empty($nilai_create)) {
                $no_create = 0;
                while ($pemain = $data->fetch_assoc()) {
                    $conn->query("INSERT INTO latihan_pemain VALUES(NULL,".$latihan_id.",".$pemain['pemain_id'].", ".$nilai_create[$no_create].")");
                    $no_create++;
                }
            }
            if (isset($nilai_update_id) && !empty($nilai_update_id)) {
                $update_id = explode(',', $nilai_update_id);
                foreach ($update_id as $key => $value) {
                    $conn->query("UPDATE latihan_pemain SET nilai=".$nilai_update[$key]." WHERE id = ".$value);
                }
            }
            $insert = true;
            $conn->commit();
        } catch (Exception $e) {
            var_dump($e);die();
            $insert = false;
            $conn->rollback();
            $conn->close();
        }
        if ($insert) {
            $response['error']   = false;
            $response['icon']    = 'success';
            $response['message'] = 'Berhasil mengubah nilai';
        } else {
            $response['error']   = true;
            $response['icon']    = 'danger';
            $response['message'] = 'Gagal mengubah nilai';
        }
    } else {
        $response['error']   = true;
        $response['icon']    = 'warning';
        $response['message'] = 'Harap memasukan nilai lewat form';
    }
    $_SESSION['alert_latihan'] = $response;
    
    header('location: ../../view/latihan/index.php');
    exit(); 
?>