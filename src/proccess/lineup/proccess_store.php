<?php
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PELATIH', 'USER']);

    $kriteriaData = getDataTable($conn, 'kriteria');
    $kriteriaArray = getDataToArray($kriteriaData, 'id');

    $latihanData = getDataTable($conn, 'latihan');
    $latihanArray = getDataToArray($latihanData, 'id');

    $latihanPemainData = getDataJoin($conn, 'latihan_pemain', 'pemain', 'pemain_id');
    $latihanPemainArray = getDataToArray($latihanPemainData, 'latihan_id');

    $pemainData = getDataTable($conn, 'pemain');
    $pemainArray = getDataToArray($pemainData, 'id');
    $posisi = getPosisi();

    $periode = date('ymdhis');
    
    if (isset($_POST)) {
        
        extract($_POST);
        try {
            $conn->autocommit(FALSE);
            $data_user = [];
            foreach ($latihan as $key => $value) {
                $latihan_model = $latihanArray[$value][0];
                // var_dump($latihanPemainArray);die();
                foreach ($latihanPemainArray as $latihan_pemains) {
                    $nilai_terbesar_terkecil = max_min_grade($latihan_pemains);
                    foreach ($latihan_pemains as $latihan_pemain) {
                        $pemain = $pemainArray[$latihan_pemain['pemain_id']][0];
                        $kriteria = $kriteriaArray[$latihan_model['kriteria_id']][0];
                        $bobot = $pemain['posisi'] == 'penyerang' ? $kriteria['attack'] : (
                            $pemain['posisi'] == 'gelandang' ? $kriteria['mid'] : (
                                $pemain['posisi'] == 'bek' ? $kriteria['defend'] : $kriteria['keep']
                            )
                        );
                        
                        // normalisasi
                        $normalisasi = normalisasi($kriteria['type'], $latihan_pemain['nilai'], $nilai_terbesar_terkecil[$latihan_pemain['posisi']]);
                        $data_user[$latihan_pemain['pemain_id']]['normalisasi'][$latihan_model['kriteria_id']] = $normalisasi;
                        $data_user[$latihan_pemain['pemain_id']]['preferensi'][$latihan_model['kriteria_id']] = $normalisasi * $bobot;
                    }
                }
                
            }
            $ranking = [];

    // var_dump($data_user);die();
            foreach ($data_user as $key => $value) {
                $total = 0;
                $pemain = $pemainArray[$key][0];

                foreach ($value['preferensi'] as $nilai) {
                    $total += $nilai;
                }
                $ranking[$pemain['posisi']][$key]['preferensi'] = $total;
                $ranking[$pemain['posisi']][$key]['pemain_id'] = $pemain['id'];
            }

            foreach ($posisi as $key => $value) {
                usort($ranking[$key], function($a, $b) {
                    return $b['preferensi'] <=> $a['preferensi'];
                });
            }
            foreach ($posisi as $key => $value) {
                foreach ($ranking[$key] as $k => $v) {
                    $conn->query("INSERT INTO perhitungan VALUES(NULL, '".$v['pemain_id']."', '".$key."', '".$v['preferensi']."', '".($k + 1)."', '".$periode."', '".date('Y-m-d H:i:s')."')");
                }
            }

            $insert = true;
            $conn->commit();
        } catch (Exception $e) {
            var_dump($e);die();
            $insert = false;
            $conn->rollback();
            $conn->close();
        }
        if ($insert) {
            $response['error']   = false;
            $response['icon']    = 'success';
            $response['message'] = 'Berhasil menggenerate data';
        } else {
            $response['error']   = true;
            $response['icon']    = 'danger';
            $response['message'] = 'Gagal menggenerate data';
        }
    } else {
        $response['error']   = true;
        $response['icon']    = 'warning';
        $response['message'] = 'Harap memasukan data lewat form';
    }
    $_SESSION['alert_lineup'] = $response;
    
    header('location: ../../view/lineup/create_lineup.php?periode='.$periode);
    exit(); 
?>