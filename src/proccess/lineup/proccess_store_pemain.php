<?php
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PELATIH', 'USER']);
    if (isset($_POST)) {
        
        extract($_POST);
        try {
            $conn->autocommit(FALSE);
            $conn->query("INSERT INTO lineup VALUES(NULL, '".htmlspecialchars($nama)."', '".htmlspecialchars($periode)."', '".date('Y-m-d H:i:s')."')");
            $lineup_id = $conn->insert_id;
            foreach ($pemain_id as $key => $value) {
                $conn->query("INSERT INTO lineup_pemain VALUES(NULL, '".$lineup_id."', '".$value."')");
            }
            $update = true;
            $conn->commit();
        } catch (Exception $e) {
            var_dump($e);die();
            $update = false;
            $conn->rollback();
            $conn->close();
        }
        if ($update) {
            $response['error']   = false;
            $response['icon']    = 'success';
            $response['message'] = 'Berhasil menambahkan data';
        } else {
            $response['error']   = true;
            $response['icon']    = 'danger';
            $response['message'] = 'Gagal menambahkan data';
        }
    } else {
        $response['error']   = true;
        $response['icon']    = 'warning';
        $response['message'] = 'Harap memasukan data lewat form';
    }
    $_SESSION['alert_lineup'] = $response;
    
    header('location: ../../view/lineup/index.php');
    exit(); 
?>