<?php
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PELATIH', 'USER']);
    
    if (isset($_POST)) {
        
        extract($_POST);
        $validusername = checkUsername($conn, $username, $_GET['id']);
        if ($validusername) {
            $new_file = null;
            if (isset($_FILES)) {
                $ekstensi      = ['png','jpg','jpeg','gif', 'pdf'];
                $nama_file     = explode('.', $_FILES['profile']['name']);
                $ekstensi_file = strtolower(end($nama_file));
                $file_tmp      = $_FILES['profile']['tmp_name'];
                if (in_array($ekstensi_file, $ekstensi)) {
                    $new_file  = date('YmdHis').'.'.$ekstensi_file;
                    move_uploaded_file($file_tmp, '../../../upload/'.$new_file);
                }
            }
    
            $ubahpass = false;
            $cansave = true;
            if (isset($password) && isset($confirm_password) && !empty($password) && !empty($confirm_password)) {
                $ubahpass = true;
                // var_dump('ssss', $_FILES);die();
                if (htmlspecialchars($password) != htmlspecialchars($confirm_password)) {
                    $cansave = false;
                }
            }
            
        // var_dump($_POST,$_GET, $_FILES);die();
    
            if ($cansave) {
                $password_change = '';
                if ($ubahpass) {
                    $password_change = ", password='".sha1(htmlspecialchars($password))."'";
                }
                $profile_change = '';
                if ($new_file != null) {
                    $profile_change = ", profile='".$new_file."'";
                }
                
                try {
                    $conn->autocommit(FALSE);
                    $conn->query("UPDATE users SET username='".htmlspecialchars($username)."', phone='".htmlspecialchars($telp)."', alamat='".htmlspecialchars($alamat)."'".$password_change.$profile_change." WHERE id=".$_GET['id']);
                    $conn->query("UPDATE pemain SET nama_pemain='".htmlspecialchars($nama)."', posisi='".htmlspecialchars($posisi)."', tempat_lahir='".htmlspecialchars($tempat_lahir)."', tanggal_lahir='".htmlspecialchars($tgl_lahir)."' WHERE user_id=".$_GET['id']);
                    $update = true;
                    $conn->commit();
                } catch (Exception $e) {
                    var_dump($e);die();
                    $update = false;
                    $conn->rollback();
                    $conn->close();
                }
                if ($update) {
                    $response['error']   = false;
                    $response['icon']    = 'success';
                    $response['message'] = 'Berhasil mengubah data';
                } else {
                    $response['error']   = true;
                    $response['icon']    = 'danger';
                    $response['message'] = 'Gagal mengubah data';
                }
            } else {
                $response['error']   = true;
                $response['icon']    = 'warning';
                $response['message'] = 'Password dan Konfirm Password harus sama';
            }
        } else {
            $response['error']   = true;
            $response['icon']    = 'warning';
            $response['message'] = 'Username sudah digunakan, harap menggunakan username lainnya';
        }
    } else {
        $response['error']   = true;
        $response['icon']    = 'warning';
        $response['message'] = 'Harap memasukan data lewat form';
    }
    $_SESSION['alert_pemain'] = $response;
    if (isset($_GET['profile']) && $_GET['profile']) {
        $redirect = 'index.php';
    } else {
        $redirect = 'pemain/index.php';
    }
    header('location: ../../view/'.$redirect);
    exit(); 
?>