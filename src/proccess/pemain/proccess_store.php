<?php
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PELATIH', 'USER']);
    // var_dump($_POST, $_FILES);die();
    if (isset($_POST)) {
        
        extract($_POST);
        $validusername = checkUsername($conn, $username);
        if ($validusername) {
            $new_file = 'noimage.jpeg';
            if (isset($_FILES)) {
                $ekstensi      = ['png','jpg','jpeg','gif', 'pdf'];
                $nama_file     = explode('.', $_FILES['profile']['name']);
                $ekstensi_file = strtolower(end($nama_file));
                $file_tmp      = $_FILES['profile']['tmp_name'];
                if (in_array($ekstensi_file, $ekstensi)) {
                    $new_file  = date('YmdHis').'.'.$ekstensi_file;
                    move_uploaded_file($file_tmp, '../../../upload/'.$new_file);
                }
            }
    
            if (htmlspecialchars($password) == htmlspecialchars($confirm_password)) {
                try {
                    $conn->autocommit(FALSE);
                    $user = $conn->query("INSERT INTO users VALUES(NULL,'".htmlspecialchars($username)."', '".sha1(htmlspecialchars($password))."', '".htmlspecialchars($telp)."', 'USER',   '".htmlspecialchars($alamat)."', '".$new_file."')");
                    $user_id = $conn->insert_id;
                    $conn->query("INSERT INTO pemain VALUES(NULL,".$user_id.", '".htmlspecialchars($nama)."', '".htmlspecialchars($posisi)."', '".htmlspecialchars($tempat_lahir)."',   '".htmlspecialchars($tgl_lahir)."')");
                    $insert = true;
                    $conn->commit();
                } catch (Exception $e) {
                    var_dump($e);die();
                    $insert = false;
                    $conn->rollback();
                    $conn->close();
                }
                if ($insert) {
                    $response['error']   = false;
                    $response['icon']    = 'success';
                    $response['message'] = 'Berhasil menambahkan data';
                } else {
                    $response['error']   = true;
                    $response['icon']    = 'danger';
                    $response['message'] = 'Gagal menambahkan data';
                }
            } else {
                $response['error']   = true;
                $response['icon']    = 'warning';
                $response['message'] = 'Password dan Konfirm Password harus sama';
            }
        } else {
            $response['error']   = true;
            $response['icon']    = 'warning';
            $response['message'] = 'Username sudah digunakan, harap menggunakan username lainnya';
        }
    } else {
        $response['error']   = true;
        $response['icon']    = 'warning';
        $response['message'] = 'Harap memasukan data lewat form';
    }
    $_SESSION['alert_pemain'] = $response;
    
    header('location: ../../view/pemain/index.php');
    exit(); 
?>