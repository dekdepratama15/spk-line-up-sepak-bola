<?php
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PELATIH', 'USER']);
    if (isset($_POST)) {
        
        extract($_POST);
        $validusername = checkUsername($conn, $username, $_GET['id']);
        if ($validusername) {
            $new_file = null;
            $foto_profile = null;
            if (isset($_FILES)) {
                $ekstensi      = ['png','jpg','jpeg','gif', 'pdf'];
                $nama_file     = explode('.', $_FILES['lisensi']['name']);
                $ekstensi_file = strtolower(end($nama_file));
                $file_tmp      = $_FILES['lisensi']['tmp_name'];
    
                $nama_file2     = explode('.', $_FILES['profile']['name']);
                $ekstensi_file2 = strtolower(end($nama_file2));
                $file_tmp2      = $_FILES['profile']['tmp_name'];
                if (in_array($ekstensi_file, $ekstensi)) {
                    $new_file  = date('YmdHis').'.'.$ekstensi_file;
                    move_uploaded_file($file_tmp, '../../../upload/'.$new_file);
                }
                if (in_array($ekstensi_file2, $ekstensi)) {
                    $profile  = (date('YmdHis')+1).'.'.$ekstensi_file2;
                    move_uploaded_file($file_tmp2, '../../../upload/'.$profile);
                }
            }
    
            $ubahpass = false;
            $cansave = true;
            if (isset($password) && isset($confirm_password) && !empty($password) && !empty($confirm_password)) {
                $ubahpass = true;
                if (htmlspecialchars($password) != htmlspecialchars($confirm_password)) {
                    $cansave = false;
                }
            }
            
    
            if ($cansave) {
                $password_change = '';
                $lisensi_change = '';
                $profile_change = '';
                if ($ubahpass) {
                    $password_change = ", password='".sha1(htmlspecialchars($password))."'";
                }
                if ($new_file != null) {
                    $lisensi_change = ", lisensi='".$new_file."'";
                }
                if ($profile != null) {
                    $profile_change = ", profile='".$profile."'";
                }
    
                try {
                    $conn->autocommit(FALSE);
                    $conn->query("UPDATE users SET username='".htmlspecialchars($username)."', phone='".htmlspecialchars($telp)."', alamat='".htmlspecialchars($alamat)."'".$password_change.$profile_change." WHERE id=".$_GET['id']);
                    $conn->query("UPDATE pelatih SET nama_pelatih='".htmlspecialchars($nama)."', jabatan='".htmlspecialchars($jabatan)."', tempat_lahir='".htmlspecialchars($tempat_lahir)."', tanggal_lahir='".htmlspecialchars($tgl_lahir)."'".$lisensi_change." WHERE user_id=".$_GET['id']);
                    $update = true;
                    $conn->commit();
                } catch (Exception $e) {
                    $update = false;
                    $conn->rollback();
                    $conn->close();
                }
                if ($update) {
                    $response['error']   = false;
                    $response['icon']    = 'success';
                    $response['message'] = 'Berhasil mengubah data';
                } else {
                    $response['error']   = true;
                    $response['icon']    = 'danger';
                    $response['message'] = 'Gagal mengubah data';
                }
            } else {
                $response['error']   = true;
                $response['icon']    = 'warning';
                $response['message'] = 'Password dan Konfirm Password harus sama';
            }
        } else {
            $response['error']   = true;
            $response['icon']    = 'warning';
            $response['message'] = 'Username sudah digunakan, harap menggunakan username lainnya';
        }
    } else {
        $response['error']   = true;
        $response['icon']    = 'warning';
        $response['message'] = 'Harap memasukan data lewat form';
    }
    $_SESSION['alert_pelatih'] = $response;
    
    if (isset($_GET['profile']) && $_GET['profile']) {
        $redirect = 'index.php';
    } else {
        $redirect = 'pelatih/index.php';
    }
    header('location: ../../view/'.$redirect);
    exit(); 
?>