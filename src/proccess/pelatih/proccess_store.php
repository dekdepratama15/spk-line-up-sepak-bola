<?php
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PELATIH', 'USER']);
    // var_dump($_POST, $_FILES);die();
    if (isset($_POST)) {
        
        extract($_POST);
        $validusername = checkUsername($conn, $username);
        if ($validusername) {
            $new_file = null;
            $profile = 'noimage.jpeg';
            if (isset($_FILES)) {
                $ekstensi      = ['png','jpg','jpeg','gif', 'pdf'];
                $nama_file     = explode('.', $_FILES['lisensi']['name']);
                $ekstensi_file = strtolower(end($nama_file));
                $file_tmp      = $_FILES['lisensi']['tmp_name'];
    
                $nama_file2     = explode('.', $_FILES['profile']['name']);
                $ekstensi_file2 = strtolower(end($nama_file2));
                $file_tmp2      = $_FILES['profile']['tmp_name'];
                if (in_array($ekstensi_file, $ekstensi)) {
                    $new_file  = date('YmdHis').'.'.$ekstensi_file;
                    move_uploaded_file($file_tmp, '../../../upload/'.$new_file);
                }
                if (in_array($ekstensi_file2, $ekstensi)) {
                    $profile  = (date('YmdHis')+1).'.'.$ekstensi_file2;
                    move_uploaded_file($file_tmp2, '../../../upload/'.$profile);
                }
            }
    
            if (htmlspecialchars($password) == htmlspecialchars($confirm_password)) {
                try {
                    $conn->autocommit(FALSE);
                    $user = $conn->query("INSERT INTO users VALUES(NULL,'".htmlspecialchars($username)."', '".sha1(htmlspecialchars($password))."', '".htmlspecialchars($telp)."', 'PELATIH',   '".htmlspecialchars($alamat)."', '".$profile."')");
                    $user_id = $conn->insert_id;
                    $conn->query("INSERT INTO pelatih VALUES(NULL,".$user_id.", '".htmlspecialchars($nama)."', '".$new_file."', '".htmlspecialchars($jabatan)."', '".htmlspecialchars($tempat_lahir)."',   '".htmlspecialchars($tgl_lahir)."')");
                    $insert = true;
                    $conn->commit();
                } catch (Exception $e) {
                    $insert = false;
                    $conn->rollback();
                    $conn->close();
                }
                if ($insert) {
                    $response['error']   = false;
                    $response['icon']    = 'success';
                    $response['message'] = 'Berhasil menambahkan data';
                } else {
                    $response['error']   = true;
                    $response['icon']    = 'danger';
                    $response['message'] = 'Gagal menambahkan data';
                }
            } else {
                $response['error']   = true;
                $response['icon']    = 'warning';
                $response['message'] = 'Password dan Konfirm Password harus sama';
            }
        } else {
            $response['error']   = true;
            $response['icon']    = 'warning';
            $response['message'] = 'Username sudah digunakan, harap menggunakan username lainnya';
        }
    } else {
        $response['error']   = true;
        $response['icon']    = 'warning';
        $response['message'] = 'Harap memasukan data lewat form';
    }
    $_SESSION['alert_pelatih'] = $response;
    
    header('location: ../../view/pelatih/index.php');
    exit(); 
?>