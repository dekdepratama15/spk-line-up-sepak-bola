<?php
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PELATIH', 'USER']);
    $data = getDataTable($conn, 'kriteria');
    if (isset($_POST)) {
        
        extract($_POST);
        try {
            $conn->autocommit(FALSE);
            $no = 0;
            while ($kriteria = $data->fetch_assoc()) {
                $penyerang = $bobot_penyerang[$no] / 100;
                $gelandang = $bobot_gelandang[$no] / 100;
                $bek = $bobot_bek[$no] / 100;
                $kiper = 0;
                $conn->query("UPDATE kriteria SET attack='".$penyerang."', mid='".$gelandang."', defend='".$bek."', keep='".$kiper."' WHERE id=".$kriteria['id']);
                $no++;
            }
            $insert = true;
            $conn->commit();
        } catch (Exception $e) {
            var_dump($e, $_FILES);die();
            $insert = false;
            $conn->rollback();
            $conn->close();
        }
        if ($insert) {
            $response['error']   = false;
            $response['icon']    = 'success';
            $response['message'] = 'Berhasil mengubah data';
        } else {
            $response['error']   = true;
            $response['icon']    = 'danger';
            $response['message'] = 'Gagal mengubah data';
        }
    } else {
        $response['error']   = true;
        $response['icon']    = 'warning';
        $response['message'] = 'Harap memasukan data lewat form';
    }
    $_SESSION['alert_bobot'] = $response;
    
    header('location: ../../view/bobot/index.php');
    exit(); 
?>