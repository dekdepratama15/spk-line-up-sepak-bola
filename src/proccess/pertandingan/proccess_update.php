<?php
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PELATIH', 'USER']);
    if (isset($_POST)) {
        
        extract($_POST);
        try {
            $conn->autocommit(FALSE);
            $conn->query("UPDATE pertandingan SET pertandingan='".htmlspecialchars($pertandingan)."', lineup_id='".$lineup_id."', tanggal='".date('Y-m-d H:i:s', strtotime($tanggal.' 00:00:00'))."', status='".$status."' WHERE id=".$_GET['id']);
            $update = true;
            $conn->commit();
        } catch (Exception $e) {
            var_dump($e);die();
            $update = false;
            $conn->rollback();
            $conn->close();
        }
        if ($update) {
            $response['error']   = false;
            $response['icon']    = 'success';
            $response['message'] = 'Berhasil mengubah data';
        } else {
            $response['error']   = true;
            $response['icon']    = 'danger';
            $response['message'] = 'Gagal mengubah data';
        }
    } else {
        $response['error']   = true;
        $response['icon']    = 'warning';
        $response['message'] = 'Harap memasukan data lewat form';
    }
    $_SESSION['alert_pertandingan'] = $response;
    
    header('location: ../../view/pertandingan/index.php');
    exit(); 
?>