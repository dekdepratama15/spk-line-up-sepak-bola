<?php
    include "../../config/connection.php";
    include "../../config/function.php";
    validSession(['ADMIN', 'PELATIH', 'USER']);
    // var_dump($_POST, $_FILES);die();
    if (isset($_POST)) {
        
        extract($_POST);
        try {
            $conn->autocommit(FALSE);
            $conn->query("INSERT INTO pertandingan VALUES(NULL, '".$lineup_id."', 'belum_bertanding', '".date('Y-m-d', strtotime($tanggal.' 00:00:00'))."', '".htmlspecialchars($pertandingan)."')");
            $insert = true;
            $conn->commit();
        } catch (Exception $e) {
            $insert = false;
            $conn->rollback();
            $conn->close();
        }
        if ($insert) {
            $response['error']   = false;
            $response['icon']    = 'success';
            $response['message'] = 'Berhasil menambahkan data';
        } else {
            $response['error']   = true;
            $response['icon']    = 'danger';
            $response['message'] = 'Gagal menambahkan data';
        }
    } else {
        $response['error']   = true;
        $response['icon']    = 'warning';
        $response['message'] = 'Harap memasukan data lewat form';
    }
    $_SESSION['alert_pertandingan'] = $response;
    
    header('location: ../../view/pertandingan/index.php');
    exit(); 
?>