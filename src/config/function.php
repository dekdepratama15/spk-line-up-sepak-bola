<?php 

    // Function
    function validSession($role) {
        session_start();
        $redirect = 'login.php';
        if (isset($_SESSION['user']) && $_SESSION['user'] != NULL) {
            $session = $_SESSION['user'];
            if ($role[0] != 'LOGIN' && !in_array($session['role'], $role)) {
                session_destroy();
                header('location: '. $redirect);
                exit();
            } else {
                if ($role[0] == 'LOGIN') {
                    header('location: index.php');
                    exit(); 
                } 
            }
        } else {
            if ($role[0] != 'LOGIN') {
                header('location: '. $redirect);
                exit();
            }
        }
    }

    function getDataTable($conn, $table)
    {
        $data = $conn->query("SELECT * FROM ".$table);
        return $data;
    }

    function getDataTableWhereLike($conn, $table, $whereKey, $demitier, $whereValue)
    {
        $data = $conn->query("SELECT * FROM ".$table." WHERE ".$whereKey." ".$demitier." '".$whereValue."' ORDER BY ".$whereKey." ASC");
        return $data;
    }

    function getDataDetail($conn, $table, $id)
    {
        $data = $conn->query("SELECT * FROM ".$table." WHERE id = ".$id." ORDER BY ".$table.".id DESC");
        return $data;
    }

    function getDataDetailForeign($conn, $table, $id, $foreign)
    {
        $data = $conn->query("SELECT * FROM ".$table." WHERE ".$foreign." = ".$id." ORDER BY ".$table.".id DESC");
        return $data;
    }

    function getDataDetailForeignOrder($conn, $table, $id, $foreign, $order, $ascdesc)
    {
        $data = $conn->query("SELECT * FROM ".$table." WHERE ".$foreign." = ".$id." ORDER BY ".$table.".".$order." ".$ascdesc);
        return $data;
    }

    function getDataJoin($conn, $table, $join, $foreign)
    {
        $data = $conn->query("SELECT ".$table.".*, ".$table.".id as ".$table."_id, ".$join.".* FROM ".$table." INNER JOIN ".$join." ON ".$join.".id = ".$table.".".$foreign." ORDER BY ".$table.".id DESC");
        return $data;
    }

    function getDataJoinWhere($conn, $table, $join, $foreign, $whereKey, $whereValue)
    {
        $data = $conn->query("SELECT ".$table.".*, ".$table.".id as ".$table."_id, ".$join.".* FROM ".$table." INNER JOIN ".$join." ON ".$join.".id = ".$table.".".$foreign." WHERE ".$whereKey." = ".$whereValue." ORDER BY ".$table.".id DESC");
        return $data;
    }

    function getDataJoinMore($conn, $table, $join, $foreign, $join2, $foreign2)
    {
        $data = $conn->query("SELECT ".$table.".*, ".$table.".id as ".$table."_id, ".$join.".*, ".$join.".id as ".$join."_id, ".$join2.".*  FROM ".$table." INNER JOIN ".$join." ON ".$join.".id = ".$table.".".$foreign." INNER JOIN ".$join2." ON ".$join2.".id = ".$table.".".$foreign2." ORDER BY ".$table.".id DESC");
        return $data;
    }

    function getDataJoinMoreLeft($conn, $table, $join, $foreign, $join2, $foreign2)
    {
        $data = $conn->query("SELECT ".$table.".*, ".$table.".id as ".$table."_id, ".$join.".*, ".$join.".id as ".$join."_id, ".$join2.".*  FROM ".$table." INNER JOIN ".$join." ON ".$join.".id = ".$table.".".$foreign." LEFT JOIN ".$join2." ON ".$table.".id = ".$join2.".".$foreign2." ORDER BY ".$table.".id DESC");
        return $data;
    }

    function getDataJoinMoreLeftForeign($conn, $table, $join, $foreign, $join2, $foreign2, $where, $id)
    {
        $data = $conn->query("SELECT ".$table.".*, ".$table.".id as ".$table."_id, ".$join.".*, ".$join.".id as ".$join."_id, ".$join2.".*  FROM ".$table." INNER JOIN ".$join." ON ".$join.".id = ".$table.".".$foreign." LEFT JOIN ".$join2." ON ".$table.".id = ".$join2.".".$foreign2." WHERE ".$where." = ".$id." ORDER BY ".$table.".id DESC");
        return $data;
    }

    function getDataJoinMoreDetail($conn, $table, $join, $foreign, $join2, $foreign2, $id)
    {
        $data = $conn->query("SELECT ".$table.".*, ".$table.".id as ".$table."_id, ".$join.".*, ".$join.".id as ".$join."_id, ".$join2.".*  FROM ".$table." INNER JOIN ".$join." ON ".$join.".id = ".$table.".".$foreign." INNER JOIN ".$join2." ON ".$join2.".id = ".$table.".".$foreign2." WHERE ".$table.".id = ".$id." ORDER BY ".$table.".id DESC");
        return $data;
    }

    function getDataJoinDetail($conn, $table, $join, $foreign, $id)
    {
        // var_dump("SELECT ".$table.".*, ".$table.".id as ".$table."_id, ".$join.".* FROM ".$table." INNER JOIN ".$join." ON ".$join.".id = ".$table.".".$foreign." WHERE ".$table."_id = ".$id);die();
        $data = $conn->query("SELECT ".$table.".*, ".$table.".id as ".$table."_id, ".$join.".* FROM ".$table." INNER JOIN ".$join." ON ".$join.".id = ".$table.".".$foreign." WHERE ".$table.".id = ".$id." ORDER BY ".$table.".id DESC");
        return $data;
    }

    function getDataFile($conn, $table, $foreign, $id)
    {
        $data = $conn->query("SELECT * FROM ".$table." WHERE ".$foreign." = ".$id);
        return $data;
    }

    function checkUsername($conn, $username, $id = 0) {
        $andWhere = '';
        if ($id != 0) {
            $andWhere = ' AND id != '.$id;
        }
        $data = $conn->query("SELECT * FROM users WHERE username = '".$username."' ".$andWhere);
        $status = $data->num_rows > 0 ? false : true;
        return $status;
    }

    function getDataToArray($array, $key = null) {
        $result = [];
        while ($data = $array->fetch_assoc()) {
            if ($key != null) {
                $result[$data[$key]][] = $data;
            } else {
                $result[] = $data;
            }
        }
        return $result;
    }

    function getPosisi() {
        $posisi = [
            'penyerang' => 'Pemain depan', 
            'gelandang' => 'Pemain tengah', 
            'bek' => 'Pemain bertahan', 
            // 'kiper' => 'Kiper'
        ];
        return $posisi;
    }

    function getTypeKriteria() {
        $type = [
            'benefit' => 'Benefit', 
            'cost' => 'Cost', 
        ];
        return $type;
    }

    function getStatusPertandingan() {
        $status = [
            'belum_bertanding' => 'Belum Bertanding', 
            'menang' => 'Menang', 
            'seri' => 'Seri',
            'kalah' => 'Kalah', 
        ];
        return $status;
    }

    function getNamaLatihan() {
        $latihan = [
            'Sprint Berulang',
            'Drill Cone', 
            'Fartlek Training', 
            'Push Up', 
            'Pull Up', 
            'Squat', 
            'Skill Juggling',
            'Passing Drills', 
            'Latihan Motivasi',
            'Latihan Game Sense', 
            'Cone Dribbling',
            'Turn & Run', 
            'Finisihing Drills', 
            'Crossing Drills', 
            'Wall Passing', 
            'Rondo', 
            'Passing Accuracy Drills', 
            'Marking Drills', 
            '1v1 Defending Skills', 
            'Positional Defending', 
            'Set Piece', 
            'Latihan Dibawah Tekanan'
        ];
        return $latihan;
    }

    function getTypeStatistik() {
        $type = [
            'assist' => 'Assist', 
            'goal' => 'Goal', 
            'start' => 'Start', 
            'yellow_card' => 'Yellow Card', 
            'red_card' => 'Red Card', 
        ];
        return $type;
    }

    function normalisasi($type, $nilai, $min_max) {
        if ($type=='benefit'){
            $result=$nilai/$min_max['max'];
        }else{
            $result=$min_max['min']/$nilai;
        }
        return $result;
    }

    function max_min_grade($latihan_pemains) {
        $data = [];
        foreach ($latihan_pemains as $key => $value) {
            $data[$value['posisi']][] = $value['nilai'];
        }
        $min_max = [];
        foreach ($data as $key => $value) {
            $min_max[$key]['min'] = min($value);
            $min_max[$key]['max'] = max($value);
        }

        return $min_max;
    }

?>