//[Data Table Javascript]

//Project:	EduAdmin - Responsive Admin Template
//Primary use:   Used only for the Data Table

$(function () {
    "use strict";

 
    // DataTable
    var table = $('#example123').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            if ( that.search() !== this.value ) {
                that
                    .search( this.value )
                    .draw();
            }
        } );
    } );
	
	
	
	
	
  }); // End of use strict